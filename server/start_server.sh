#!/bin/bash

NODE=node

if [ `which $NODE | wc -l` -eq 0 ]
then
	NODE=nodejs
fi

function start_revis {
	which redis-server
	R=`echo $?`
	if [ $R -eq 0 ]
	then
		redis-server &
	else
		redis/redis-server.exe &
	fi
}

start_revis
sleep 1
$NODE backend_server.js &
sleep 1
$NODE service_server.js &
