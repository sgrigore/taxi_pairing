var MAX_DISTANCE = 10000; // 10000 m = 10 km
var SERVER_REST_PORT = 8080;
var MESSENGER_CHANNEL = 'queue';
var MESSENGER_PORT = 8000;

var Barrier =  require("./barrier");
var Database = require("./database.js");
var facebook = require("./facebook");

var redis = require("redis");
var crypto = require("crypto");
var qs = require('querystring');
var geolib = require("geolib");
var cluster = require('cluster');
var http = require('http');
var messenger = require('messenger');

var numCPUs = 2;// require('os').cpus().length;

var redisClient = redis.createClient();
var messengerClient;

var database;

function sendMessage(message, socket)
{
	var content = JSON.stringify(message);
	var length = content.length;
	var buffer = new Buffer(4 + length);

	buffer.writeUInt32BE(length, 0);
	buffer.write(content, 4, length, "utf8");

	socket.write(buffer);
}

function startsWith (str, prefix)
{
	return str.slice(0, prefix.length) === prefix;
}

function getFriendsIds(token, path, friends, callback)
{
	var getFriends = function(data)
	{
		var response = JSON.parse(data);
		if (response === null || !response.hasOwnProperty('data'))
		{
			console.log("facebook no data");
			callback(friends);
		}
		else
		{
			friends.concat(response.data);
			if (response.hasOwnProperty('paging') && response.paging.hasOwnProperty('next'))
			{
				console.log("facebook next");
				getFriendsIds(null, response.paging.next, friends, callback);
			}
			else
			{
				console.log("facebook end");
				callback(friends);
			}
		}
	};
		
	if (token !== null)
	{
		facebook.get(token, '/me/friends', getFriends);
	}
	else if (path !== null)
	{
		facebook.getPath(path, getFriends);
	}
	else
	{
		callback(friends);
	}

}

function sendResponse(response, message)
{
	response.writeHead(200, { 'Content-Type': 'application/json' });
	response.write(JSON.stringify(message));
	response.end();
}

function getCandidates(coords, token, callback)
{
	var srcCoords = { latitude: coords.lat, longitude: coords.long };
	var candidates = [];
	var mainF = function(friendName)
	{
		console.log(friendName);
		var distance = {};
		redisClient.hgetall('taxi',
			function (err, reply)
			{
				if (reply === null)
				{
					callback(candidates);
					return;
				}

				for (var id in reply)
				{
					if (reply.hasOwnProperty(id))
					{
						var client = JSON.parse(reply[id]);
						if (client.active && client.coords !== undefined)
						{
							var destCoords = client.coords;
							var dist = geolib.getDistance(
									srcCoords,
									{ latitude: destCoords.lat, longitude: destCoords.long }
								);

							if (dist < MAX_DISTANCE)
							{
								candidates.push({ id: id, username: client.regNumber, coords: client.coords, reviews: [] });
								distance[client.id] = dist;
							}
						}
					}
				}

				var length = candidates.length;
				if (length === 0)
				{
					callback(candidates);
					return;
				}

				var barrier = new Barrier(length,
					function ()
					{
						candidates.sort(
							function (a, b)
							{
								var rev1 = a.reviews.length;
								var rev2 = b.reviews.length;
								if (rev1 === rev2)
								{
									return distance[a.id] - distance[b.id];
								}
								else
								{
									return rev2 - rev1;
								}
							}
						);

						/* keep only 10 elements */
						candidates.slice(0, 10);

						callback(candidates);
					},
					null
				);

				var getReviewsCallback = function (reviews, candidate)
				{
					candidate.reviews = reviews;
					barrier.submit();
				};

				for (var i = 0; i < length; i += 1)
				{
					var candidate = candidates[i];
					database.getReviews(candidate.id, friendName,
						getReviewsCallback, candidate);
				}
			}
		);
	};

	facebook.get(token, '/me', function (data)
	{
		var friendName = {};
		var me = JSON.parse(data);
		if (me === null) {
			return;
		}
		friendName[me.id - 0] = me.name;
		getFriendsIds(token, null, [],
			function(friendList)
			{
				for (var i = 0; i < friendList.length; i = i + 1)
				{
					var friend = friendList[i];
					friendName[friend.id - 0] = friend.name;
				}
				mainF(friendName);
			}
		);
	});

}

function handleGetNearbyTaxi(query, response)
{
	var coords = query;
	var token_start = coords.indexOf("/");
	var token = null;
	if (token_start !== -1)
	{
		token = coords.substring(token_start + 1);
		coords = coords.substring(0, token_start);
	}
	var components = coords.split(':');
	coords =
	{
		lat: (components[0] - 0),
		long: (components[1] - 0),
		alt: (components[2] - 0)
	};
	
	console.log('Token ' + token);

	getCandidates(coords, token,
		function (candidateList)
		{
			sendResponse(response, candidateList);
		}
	);
	
}

function createRequest(srcCoords, destCoords, idList, key, response)
{
	crypto.randomBytes(32,
		function(ex, buf)
		{
			var rid = buf.toString('hex');

			var requestMessage =
			{
				rid: rid,
				src: srcCoords,
				dest: destCoords,
				ids: idList
			};
			
			var requestInfo =
			{
				status: 'PENDING',
				ids: JSON.stringify(idList),
				key: key,
				ready: 0,
			};

			/* update Redis database */
			var requestKey = 'request:' + rid;
			redisClient.hmset(requestKey, requestInfo, redis.print);
			/* set a 5 minutes time to live */
			redisClient.expire(requestKey, 300);

			/* schedule timeout activity */
			setTimeout(
				function ()
				{
					redisClient.hincrby(requestKey, 'ready', 1,
						function (err, reply)
						{
							if (reply === 1)
							{
								redisClient.hset(requestKey, 'status',
										'NO_RESPONSE', redis.print);
							}
						}
					);
				},
			60000); /* 60s */

			/* send message to the master back end server */
			messengerClient.request(MESSENGER_CHANNEL, requestMessage);

			response.writeHead(200, { 'Content-Type': 'application/json' });
			response.write(JSON.stringify({ rid: rid}));
			response.end();
		});
	
}

function handleMakeRequest(query, response)
{
	var end = query.indexOf('/');
	var coords = query.substring(0, end);
	var components = coords.split(':');
	var srcCoords =
	{
		lat: (components[0] - 0),
		long: (components[1] - 0),
		alt: (components[2] - 0)
	};
	query = query.substring(end + 1);
	end = query.indexOf('/');
	var destCoords = null;
	if (!startsWith(query, 'taxi'))
	{
		coords = query.substring(0, end);
		components = coords.split(':');
		destCoords =
		{
			lat: (components[0] - 0),
			long: (components[1] - 0),
			alt: (components[2] - 0)
		};
		query = query.substring(end + 1);
	}
	end = query.indexOf('/');
	query = query.substring(end + 1);
	
	var idList = [];
	while ((end = query.indexOf('/')) !== -1)
	{
		idList.push(query.substring(0, end));
		query = query.substring(end + 1);
	}
	var key = query;
	
	console.log('src ' + JSON.stringify(srcCoords));
	console.log('dest ' + JSON.stringify(destCoords));
	console.log('ids ' + JSON.stringify(idList));
	console.log('key ' + key);
	createRequest(srcCoords, destCoords, idList, key, response);
}

function handleCheckStatus(query, response)
{
	var end = query.indexOf('/');
	var rid;
	var key;
	var facebookId;
	if (end === -1)
		
	{
		rid = query;
		key = null;
		facebookId = null;
	}
	else
	{
		rid = query.substring(0, end);
		key = query.substring(end + 1);
		end = key.indexOf('/');
		if (end === -1)
		{
			facebookId = null;
		}
		else
		{
			facebookId = key.substring(end + 1);
			key = key.substring(0, end);
		}
	}
	
	var message = {};
	if (key === null)
	{
		message.status = 'UNKNOWN';
		sendResponse(response, message);
		return;
	}
	
	var requestKey = 'request:' + rid;
	redisClient.hgetall(requestKey,
		function (err, reply)
		{
			if (reply === null)
			{
				message.status = 'UNKNOWN';
				sendResponse(response, message);
				return;
			}

			if (reply.key !== key)
			{
				message.status = 'UNKNOWN';
				sendResponse(response, message);
				return;
			}
			
			if (reply.status !== 'READY')
			{
				message.status = reply.status;
				sendResponse(response, message);
				return;
			}
			
			var picked = reply.picked;
			redisClient.hget('taxi', picked,
				function (err, reply)
				{
					if (reply === null)
					{
						message.status = 'NO_RESPONSE';
						sendResponse(response, message);
						return;
					}
					
					var info = JSON.parse(reply);
					message.status = 'READY';
					message.taxiId = picked - 0;
					message.regNumber = info.regNumber;

					crypto.randomBytes(32,
						function(ex, buf)
						{
							var locationKey = buf.toString('hex');
							message.locationKey = locationKey;
							locationKey = 'location:' + locationKey;

							/* add location key to Redis database */
							redisClient.set(locationKey, picked);
							redisClient.expire(requestKey, 3600);

							if (facebookId === null)
							{
								sendResponse(response, message);
							}
							else
							{
								database.addEmptyReview(message.taxiId, facebookId - 0,
									function (reviewId)
									{
										message.reviewId = reviewId;
										sendResponse(response, message);
									}
								);
							}
						}
					);
				}
			);
		}
	);
}

function handleQueryLocation(query, response)
{
	var locationKey = 'location:' + query;
	var message = {};
	redisClient.get(locationKey,
		function (err, reply)
		{
			if (reply === null)
			{
				message.status = 'UNKNOWN';
				sendResponse(response, message);
				return;
			}
			
			redisClient.hget('taxi', reply,
				function (err, reply)
				{
					if (reply === null)
					{
						message.status = 'NO_INFO';
						sendResponse(response, message);
						return;
					}

					var info = JSON.parse(reply);
					message.status = 'OK';
					message.coords = info.coords;
					sendResponse(response, message);
				}
			);
		}
	);
}

function handleGetRequest(uri, response)
{
	var GET_NEARBY_TAXI = '/client/get_nearby_taxi/';
	var MAKE_REQUEST = '/client/make_request/';
	var CHECK_STATUS = '/client/check_status/';
	var QUERY_LOCATION = '/client/query_location/';
	if (startsWith(uri, GET_NEARBY_TAXI))
	{
		handleGetNearbyTaxi(uri.substring(GET_NEARBY_TAXI.length), response);
	}
	else if (startsWith(uri, MAKE_REQUEST))
	{
		handleMakeRequest(uri.substring(MAKE_REQUEST.length), response);
	}
	else if (startsWith(uri, CHECK_STATUS))
	{
		handleCheckStatus(uri.substring(CHECK_STATUS.length), response);
	}
	else if (startsWith(uri, QUERY_LOCATION))
	{
		handleQueryLocation(uri.substring(QUERY_LOCATION.length), response);
	}
	else if (uri === '/')
	{
		response.writeHead(200, { 'Content-Type': 'text/plain' });
		response.write('It Works!');
		response.end();
	}
}

function handleAddReview(key, request, response)
{
	var body = '';
	request.on('data',
		function (data)
		{
			body += data;
		}
	);
	request.on('end',
		function()
		{
			var post =  qs.parse(body);
			database.updateReview(key, post.content,
				function (status)
				{
					var message = {};
					if (status)
					{
						message.status = 'OK';
					}
					else
					{
						message.status = 'FAIL';
					}

					response.writeHead(200, { 'Content-Type': 'application/json' });
					response.write(JSON.stringify(message));
					response.end();
				}
			);
		}
	);
}

function handlePostRequest(request, response)
{
	var ADD_REVIEW = '/client/add_review/';
	var uri = request.url;
	if (startsWith(uri, ADD_REVIEW))
	{
		handleAddReview(uri.substring(ADD_REVIEW.length), request, response);
	}
}

function createServer()
{
	http.createServer(
		function (request, response)
		{
			if (request.method === 'GET')
			{
				handleGetRequest(request.url, response);
			}
			else if (request.method === 'POST')
			{
				handlePostRequest(request, response);
			}
			console.log(request.method);
			console.log(request.url);
		}
	).listen(SERVER_REST_PORT,
		function ()
		{
			messengerClient = messenger.createSpeaker(MESSENGER_PORT);
			console.log('service server instance ready');
		}
	);
}

function ServiceServer()
{
	database = new Database(createServer);
}

if (cluster.isMaster)
{
	for (var i = 0; i < numCPUs; i += 1)
	{
		cluster.fork();
	}
	
	cluster.on('exit',
		function (worker, code, signal)
		{
			cluster.fork();
		}
	);
}
else
{
	var serviceServer = new ServiceServer();
}
