
var CLIENT_ID = "816739718909-5eu116cqbd0iiibv035mq77kn1q0op8f@developer.gserviceaccount.com";
var DATASET_ID = "taxi-service-000";
var PRIVATE_KEY_PATH = 'key/privateKey.pem';

var googleapis = require('googleapis');
var googleAuth = require("google-oauth-jwt");
var events = require('events');
var util = require('util');
var OAuth2Client = googleapis.OAuth2Client;
var crypto = require('crypto');

function DatastoreLayer(datasetId, callback)
{
	this.datasetId = datasetId;
	this.authClient = new OAuth2Client(CLIENT_ID, "", "");
	this.authClient.credentials = {};
	
	var datastoreLayer = this;
	googleAuth.authenticate(
		{
			email: CLIENT_ID,
			keyFile: PRIVATE_KEY_PATH,
			scopes: ['https://www.googleapis.com/auth/datastore',
					'https://www.googleapis.com/auth/userinfo.email']
		},
		function (err, token)
		{
			datastoreLayer.authClient.credentials.token_type = "Bearer";
			datastoreLayer.authClient.credentials.access_token = token;
			datastoreLayer.connect(callback);
		}
	);
}
util.inherits(DatastoreLayer, events.EventEmitter);

DatastoreLayer.prototype.connect = function(callback)
{
	googleapis.discover('datastore', 'v1beta2')
		.execute(
			function(err, client)
			{
				if (err)
				{
					this.emit('error', err);
					return;
				}
				this.datastore = client.datastore.withDefaultParams(
					{datasetId: this.datasetId}).datasets;
				callback();
			}.bind(this)
		);
};

DatastoreLayer.prototype.runQuery = function(query, callback)
{
	this.datastore.runQuery(query)
		.withAuthClient(this.authClient)
		.execute(
			function(err, result)
			{
				if (err)
				{
					console.log(err);
					//this.emit('error', err);
					return;
				}

				if (result.batch.entityResults)
				{
					callback(result.batch.entityResults);
				}
				else
				{
					callback(null);
				}
			}
		);
};

DatastoreLayer.prototype.createEntity = function(entity, callback)
{
	this.datastore
		.commit(
		{
			mutation: { insertAutoId: [entity] },
			mode: 'NON_TRANSACTIONAL'
		})
		.withAuthClient(this.authClient)
		.execute(
		function(err, result)
		{
			console.log("ERROR " + JSON.stringify(err));
			if (callback !== null)
			{
				callback(result);
			}
		}
	);
};

DatastoreLayer.prototype.retrieveEntity = function(key, callback)
{
	this.datastore.lookup({ keys: [key] })
		.withAuthClient(this.authClient)
		.execute(
			function(err, result)
			{
				if (!err)
				{
					var entity = null;
					if (result.found && result.found.length !== 0)
					{
						entity = result.found[0].entity;
						callback(entity);
					}
					else
					{
						callback(null);
					}
				}
			}
		);
};

DatastoreLayer.prototype.deleteEntity = function(key)
{
	this.datastore.commit(
		{
			mutation: { 'delete': [key] },
			mode: 'NON_TRANSACTIONAL'
		})
		.withAuthClient(this.authClient)
		.execute(
		function(err, result)
		{
		}
		);
};

function Database(callback)
{
	this.datastoreLayer = new DatastoreLayer(DATASET_ID, callback);
}

Database.prototype.getReviews = function(taxiId, ids, callback, arg)
{
	var taxiDriver = { key: { path: [{ kind: 'TaxiDriver', id: taxiId }] } };
	var taxiDriverKey = taxiDriver.key;
	
	var idList = Object.keys(ids);
	if (ids === null || idList.length === 0)
	{
		callback([], arg);
		return;
	}

	var minId = (idList[0] - 0);
	var maxId = minId;
	for (var i = 1;  i < idList.length; i = i + 1)
	{
		var id = (idList[i] - 0);
		if (id < minId)
		{
			minId = id;
		}
		if (id > maxId)
		{
			maxId = id;
		}
	}

	console.log(maxId + " " + minId);
	var query =
	{
		query:
		{
			kinds: [{name: 'Review' }],
			filter:
			{
				compositeFilter:
				{
					operator: 'AND',
					filters:
					[
						{
							propertyFilter:
							{
								property: { name: '__key__' },
								operator: 'HAS_ANCESTOR',
								value: { keyValue: taxiDriverKey }
							}
						},
						{
							propertyFilter:
							{
								property: { name: 'facebookId' },
								operator: 'GREATER_THAN_OR_EQUAL',
								value: { integerValue: minId }
							}
						},
						{
							propertyFilter:
							{
								property: { name: 'facebookId' },
								operator: 'LESS_THAN_OR_EQUAL',
								value: { integerValue: maxId }
							}
						}
					]
				}
			}
		}
	};
	
	this.datastoreLayer.runQuery(query,
		function (results)
		{
			var reviews = [];
			for (var i = 0; i < results.length; i = i + 1)
			{
				var properties = results[i].entity.properties;
				var facebookId = properties.facebookId.integerValue;
				if (ids[facebookId] !== undefined)
				{
					var review =
					{
						fuid: facebookId - 0,
						name: ids[facebookId],
						content: properties.content.stringValue
					};
					reviews.push(review);
				}
			}
			console.log('RESULTS ' + JSON.stringify(reviews));
			callback(reviews, arg);
		}
	);
};

Database.prototype.addReview = function(taxiId, facebookId, content)
{
	var entity =
	{
		key: { path: [{ kind: 'TaxiDriver', id: taxiId }, { kind: 'Review' }] },
		properties:
		{
			facebookId: { integerValue: (facebookId - 0) },
			content: { stringValue: content },
		}
	};
	
	this.datastoreLayer.createEntity(entity, null);
};

Database.prototype.addEmptyReview = function(taxiId, facebookId, callback)
{
	var entity =
	{
		key: { path: [{ kind: 'EmptyReview' }] },
		properties:
		{
			taxiId: { integerValue: (taxiId - 0) },
			facebookId: { integerValue: (facebookId - 0) },
			timestamp: { integerValue: (new Date()).getTime() },
		}
	};
	
	this.datastoreLayer.createEntity(entity,
		function(result)
		{
			callback(result.mutationResult.insertAutoIdKeys[0].path[0].id);
		}
	);
};

Database.prototype.checkUser = function(username, password, callback)
{
	var filters =
	{
		propertyFilter:
		{
			property: { name: 'username' },
			operator: "equal",
			value:
			{
				stringValue: username
			}
		}
	};
	
	/*
	if (username === 'gicutu')
	{
		this.addReview('5707702298738688', '100000532443329', 'Baiat bun, dar cam fura la aparat.');
		this.addReview('5707702298738688', '100001441482104', 'Conduce cam haotic, se crede meserias si trece doar pe rosu la semafor.');
	}

	if (username === 'petrica')
	{
		this.addReview('5741031244955648', '100004914793435', 'Rapid si politicos.');
	}
	*/
	/*
	if (username === 'bebe')
	{
		this.addEmptyReview(24324324234, 234324234);
	}
	*/
	
	var query =
	{
		query:
		{
			kinds: [{name: 'TaxiDriver' }],
			filter: filters,
		}
	};
	
	this.datastoreLayer.runQuery(query,
		function (results)
		{
			if (results === null || results.length === 0)
			{
				callback(false, null);
				return;
			}

			var entity = results[0].entity;
			var properties = entity.properties;
			var salt = properties.salt.integerValue;
			var digest = properties.digest.stringValue;
			
			var encrypted = crypto.createHmac('sha1', salt)
				.update(password)
				.digest('hex');
			
			if (encrypted === digest)
			{
				var info =
				{
					regNumber: properties.regNumber.stringValue,
					id: entity.key.path[0].id
				};
				callback(true, info);
			}
			else
			{
				callback(false, null);
			}
		}
	);
};

Database.prototype.updateReview = function(emptyReviewId, content, callback)
{
	var key = { path: [{ kind: 'EmptyReview', id: emptyReviewId }] };
	var datastoreLayer = this.datastoreLayer;
	var database = this;
	this.datastoreLayer.retrieveEntity(key,
		function (entity)
		{
			if (entity === null)
			{
				callback(false);
			}
			else
			{
				console.log('ent ' + JSON.stringify(entity));
				var properties = entity.properties;
				var taxiId = properties.taxiId.integerValue;
				var facebookId = properties.facebookId.integerValue;
				
				console.log('fid ' + facebookId);
				
				var emptyReviewKey = { path: [{ kind: 'EmptyReview', id: emptyReviewId }] };
				datastoreLayer.deleteEntity(emptyReviewKey);
				database.addReview(taxiId, facebookId, content);
				
				callback(true);
			}
		
		}
	);
};

module.exports = Database;

