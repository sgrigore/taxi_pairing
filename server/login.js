var http = require('http');
var fs = require('fs');
var LOGIN_SERVER_PORT = 8081;


http.createServer(
		function (request, response)
		{
			var filename = "login.html";
			var fileStream = fs.createReadStream(filename);
			fileStream.on('data',
				function (data)
				{
					response.write(data);
				}
			);
			fileStream.on('end',
				function()
				{
					response.end();
				}
			);
		}
	).listen(LOGIN_SERVER_PORT);