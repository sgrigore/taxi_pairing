var HEADER_LENGTH = 4;
var SERVER_TAXI_PORT = 3333;
var MESSENGER_PORT = 8000;
var MESSENGER_CHANEL = 'queue';

var net = require('net');
var redis = require("redis");
var messenger = require('messenger');

/* mapping between socket keys and ids */
var clients = {};
/* mapping between ids and sockets */
var sockets = {};

exports.MESSENGER_PORT = MESSENGER_PORT;
exports.MESSENGER_CHANNEL = MESSENGER_CHANEL;

var Database = require("./database.js");
var Client = require("./client.js");
var redisClient = redis.createClient();

var cluster = require('cluster');
var numCPUs = 2;// require('os').cpus().length;

var database;

function sendMessage(message, socket)
{
	var content = JSON.stringify(message);
	var length = content.length;
	var buffer = new Buffer(4 + length);

	buffer.writeUInt32BE(length, 0);
	buffer.write(content, 4, length, "utf8");

	socket.write(buffer);
}

function removeConnections(workerKey)
{
	redisClient.hgetall('taxi',
		function (err, reply)
		{
			if (reply === null)
			{
				return;
			}
	
			for(var key in reply)
			{
				if(reply.hasOwnProperty(key))
				{
					var info = JSON.parse(reply[key]);
					if (info.workerKey === workerKey)
					{
						redisClient.hdel('taxi', key, redis.print);
					}
				}
				
			}
		}
	);
}


function createServer()
{
	net.createServer(
		function (socket)
		{
			var buffer = new Buffer(0);
			var receivedBytes = 0;
			var totalLength = -1;

			function socketKey(socket)
			{
				return socket.remoteAddress + '#' + socket.remotePort;
			}

			function checkAlreadyConnected(key, callback)
			{
				redisClient.hkeys('taxi',
					function (err, reply)
					{
						if (reply === null)
						{
							callback(false);
							return;
						}
				
						for (var i = 0; i < reply.length; i += 1)
						{
							var id = reply[i];
							var end = id.indexOf(':');
							if (end === -1)
							{
								continue;
							}

							if (id.substring(0, end) === key)
							{
								callback(true);
								return;
							}

						}

						callback(false);
					}
				);
			}
			
			function handleMessage(buffer, socket)
			{
				var message = JSON.parse(buffer);
				var info;
				
				console.log('server ' + cluster.worker.id + ': ' + JSON.stringify(message));

				/* authenticate taxi application client */
				if (message.type === "login")
				{
					database.checkUser(message.username, message.password,
						function (ok, info)
						{
							var response;
							if (ok)
							{
								var id = info.id;
								var worker = cluster.worker;
								var workerKey = worker.id + ':' + worker.process.pid;
								var value =
								{
									active: true,
									regNumber: info.regNumber,
									workerKey: workerKey
								};

								redisClient.hsetnx("taxi", id, JSON.stringify(value),
									function (err, result)
									{
										if (result)
										{
											response = { type: 'login_ack' };

											var sockKey = socketKey(socket);
											socket.sockKey = sockKey;
											clients[sockKey] = id;
											sockets[id] = socket;
										}
										else
										{
											response =
											{
												type: 'login_nack',
												reason: 'Already connected'
											};
										}

										sendMessage(response, socket);
									}
								);
							}
							else
							{
								response =
								{
									type: 'login_nack',
									reason: 'Invalid username/password'
								};
								sendMessage(response, socket);
							}
						}
					);
				}
				/* update taxi application client location */
				else if (message.type === 'update_location')
				{
					var id = clients[socketKey(socket)];
					
					if (id === undefined)
					{
						/* there is no such client */
						return;
					}

					var worker = cluster.worker;
					/* update Redis database */
					redisClient.hget('taxi', id,
						function (err, reply)
						{
							if (reply === null)
							{
								/* there is no such client */
								return;
							}

							var info = JSON.parse(reply);
							info.coords = message.coords;
							redisClient.hset('taxi', id, JSON.stringify(info), redis.print);
						}
					);
				}
				else if (message.type === 'serve_order')
				{
					var clientId = clients[socketKey(socket)];
					
					if (clientId === undefined)
					{
						/* there is no such client */
						return;
					}
					
					var response =
					{
						type: 'order_confirmation',
						rid: message.rid
					};

					if (message.response === 'accept')
					{
						var key = 'request:' + message.rid;
						redisClient.hget(key, 'ids',
							function (err, reply)
							{
								if (reply === null)
								{
									response.response = 'reject';
									sendMessage(response, socket);
									return;
								}
								var ids = JSON.parse(reply);
								if (ids.indexOf(clientId) === -1)
								{
									response.response = 'reject';
									sendMessage(response, socket);
									return;
								}
								
								redisClient.hincrby(key, 'ready', 1,
									function (err, reply)
									{
										if (reply === 1)
										{
											redisClient.hmset(key, 'status', 'READY',
													'picked', clientId,
													redis.print);
											response.response = 'accept';
											sendMessage(response, socket);
										}
										else
										{
											response.response = 'reject';
											sendMessage(response, socket);
										}
									}
								);
							}
						);
					}
					else
					{
						response.response = 'reject';
						sendMessage(response, socket);
					}
				}
			}

			function removeClient(socket)
			{
				var key;
				if (socket.hasOwnProperty("sockKey"))
				{
					key = socket.sockKey;
				}
				else
				{
					key = socketKey(socket);
				}
				console.log('clients ' + JSON.stringify(clients) + "---" + key);
				var id = clients[key];
				if (id === null)
				{
					return;
				}

				delete sockets[id];
				delete clients[key];
				redisClient.hdel('taxi', id, redis.print);
			}

			socket.on('error',
				function ()
				{
					console.log('server ' + cluster.worker.id + ': ' + 'client exit on close');
					removeClient(socket);
				}
			);
			
			socket.on('end',
					function ()
					{
						console.log('server ' + cluster.worker.id + ': ' + 'client exit on end');
						removeClient(socket);
					}
				);
			
			socket.on('close',
				function ()
				{
					console.log('server ' + cluster.worker.id + ': ' + 'client exit on close');
					removeClient(socket);
				}
			);

			socket.on('data',
				function (chunk)
				{
					var chunkLenght = chunk.length;
					var tmpBuffer = new Buffer(receivedBytes + chunkLenght);
					buffer.copy(tmpBuffer);
					chunk.copy(tmpBuffer, receivedBytes);
					buffer = tmpBuffer;
					receivedBytes += chunkLenght;

					if (receivedBytes >= HEADER_LENGTH)
					{
						if (totalLength < 0)
						{
							totalLength = buffer.readUInt32BE(0) + HEADER_LENGTH;
						}

						var remainingBytes = receivedBytes;
						var offset = 0;
						while (remainingBytes >= totalLength)
						{
							var messageLength = totalLength - HEADER_LENGTH;
							var message = new Buffer(messageLength);
							buffer.copy(message, 0, offset + HEADER_LENGTH, offset + totalLength);

							handleMessage(message, socket);

							remainingBytes -= totalLength;
							offset += totalLength;
							if (remainingBytes >= HEADER_LENGTH)
							{
								totalLength = buffer.readUInt32BE(offset) + HEADER_LENGTH;
							}
							else
							{
								totalLength = -1;
								break;
							}
						}

						if (offset > 0)
						{
							receivedBytes -= offset;
							var newBuffer = new Buffer(receivedBytes);
							buffer.copy(newBuffer, 0, offset);
							buffer = newBuffer;
						}
					}
				}
			);
		}
		
	).listen(SERVER_TAXI_PORT,
		function ()
		{
			console.log('backend server instance ready');
		}
	);
}

function handleRequestMessage(message)
{
	var ids = message.ids;
	var clientMessage =
	{
		type: 'serve_order',
		rid: message.rid,
		src: message.src,
		dest: message.dest,
	};

	for (var i = 0; i < ids.length; i += 1)
	{
		var socket = sockets[ids[i]];
		if (socket !== undefined && socket !== null)
		{
			sendMessage(clientMessage, socket);
		}
	}
}

function BackendServer()
{
	database = new Database(createServer);
}

if (cluster.isMaster)
{
	var workers = {};

	var messengerServer = messenger.createListener(MESSENGER_PORT);
	messengerServer.on(MESSENGER_CHANEL,
		function (message, data)
		{
			for (var workerId in workers)
			{
				if (workers.hasOwnProperty(workerId))
				{
					var worker = workers[workerId];
					if (worker !== undefined && worker !== null)
					{
						worker.send(data);
					}
				}
			}
		}
	);
	
	for (var i = 0; i < numCPUs; i += 1)
	{
		var worker = cluster.fork();
		workers[worker.id] = worker;
	}
	
	cluster.on('exit',
		function (worker, code, signal)
		{
			delete workers[worker.id];
			var workerKey = worker.id + ':' + worker.process.pid;
			removeConnections(workerKey);

			/* create new worker */
			worker = cluster.fork();
			workers[worker.id] = worker;
		}
	);
	
	
	setTimeout(
		function ()
		{
			var gicutu = new Client('gicutu', 'gicutu', { lat: 44.44101, long: 26.13313, alt: 0.0});
			var bebe = new Client('bebe', 'bebe', { lat: 44.45311, long: 26.10467, alt: 0.0});
			var petrica = new Client('petrica', 'petrica', { lat: 44.44475, long: 26.05442, alt: 0.0});
			//gicutu = new Client('gicutu', 'gicutu', { lat: 44.44101, long: 26.13313, alt: 0.0});
			//gicutu = new Client('gicutu', 'gicutu', { lat: 44.44101, long: 26.13313, alt: 0.0});
		},
	10000);
	
}
else
{
	var backendServer = new BackendServer();
	process.on('message',
		function (message)
		{
			handleRequestMessage(message);
			console.log('worker ' + cluster.worker.id + ' ' + JSON.stringify(message));
		}
	);
}


