
var net = require('net');
var SERVER_HOST = 'localhost';
var SERVER_PORT = 3333;
var HEADER_LENGTH = 4;

var socket = new net.Socket();
function Client(username, password, coords)
{
	this.username = username;
	this.password = password;
	this.coords = coords;
	this.connect();
}

Client.prototype.connect = function()
{
	this.socket = new net.Socket();
	
	function sendMessage(message, socket)
	{
		var content = JSON.stringify(message);
		var length = content.length;
		var buffer = new Buffer(4 + length);

		buffer.writeUInt32BE(length, 0);
		buffer.write(content, 4, length, "utf8");

		socket.write(buffer);
	}
	
	function updatePosition(response, socket)
	{
		sendMessage(response, socket);
		/*
		setTimeout(
			function ()
			{
				updatePosition(response, socket);
			},
		5000);
		*/
	}
	
	var client = this;
	this.socket.connect(SERVER_PORT, SERVER_HOST,
		function()
		{
			var message =
			{
				type: 'login',
				username: client.username,
				password: client.password
			};
			sendMessage(message, client.socket);
		}
	);
	
	function handleMessage(buffer, socket)
	{
		var message = JSON.parse(buffer);
		console.log('client [' +  client.username + ']:' + JSON.stringify(message));
		if (message.type === "login_ack")
		{
			var response =
			{
				type: 'update_location',
				coords: client.coords
			};
			
			updatePosition(response, client.socket);
		}
		else if (message.type === "serve_order")
		{
			var resp =
			{
				type: 'serve_order',
				rid: message.rid
			};
			if (client.username === "gicutu")
			{
				resp.response = "accept";
			}
			else
			{
				resp.response = "reject";
			}
			sendMessage(resp, client.socket);
		}
	}
	
	var buffer = new Buffer(0);
	var receivedBytes = 0;
	var totalLength = -1;
	this.socket.on('data',
		function (chunk)
		{
		
			var chunkLenght = chunk.length;
			var tmpBuffer = new Buffer(receivedBytes + chunkLenght);
			buffer.copy(tmpBuffer);
			chunk.copy(tmpBuffer, receivedBytes);
			buffer = tmpBuffer;
			receivedBytes += chunkLenght;
			
			if (receivedBytes >= HEADER_LENGTH)
			{
				if (totalLength < 0)
				{
					totalLength = buffer.readUInt32BE(0) + HEADER_LENGTH;
				}

				var remainingBytes = receivedBytes;
				var offset = 0;
				while (remainingBytes >= totalLength)
				{
					var messageLength = totalLength - HEADER_LENGTH;
					var message = new Buffer(messageLength);
					buffer.copy(message, 0, offset + HEADER_LENGTH, offset + totalLength);
					
					handleMessage(message, socket);
					
					remainingBytes -= totalLength;
					offset += totalLength;
					if (remainingBytes >= HEADER_LENGTH)
					{
						totalLength = buffer.readUInt32BE(offset) + HEADER_LENGTH;
					}
					else
					{
						totalLength = -1;
						break;
					}
				}
				
				if (offset > 0)
				{
					receivedBytes -= offset;
					var newBuffer = new Buffer(receivedBytes);
					buffer.copy(newBuffer, offset);
					buffer = newBuffer;
				}
			}
		}
	);
};

module.exports = Client;
