package ro.pub.cs.taxi.facebook;

import java.net.URL;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.Handler;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;

public class FacebookPictureMarker {

	private final Context context;
	private final Marker marker;
	private String fuid;
	private boolean ONLY_ON_WIFI = true;
	
	public FacebookPictureMarker(Context context, Marker mk, String fuid) {
		this.context = context;
		this.marker = mk;
		setFuid(fuid);
	}

	public void setFuid(final String fuid) {
		if (fuid == null) {
			this.fuid = fuid;
			return;
		}
		if (fuid.equals(this.fuid)){
			return;
		}
		
		if (ONLY_ON_WIFI ) {
			ConnectivityManager man = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
			boolean wifi = man.getActiveNetworkInfo().getTypeName().equalsIgnoreCase("WIFI");
			if (wifi == false) {
				return;
			}
		}
		
		new Thread() {
			@Override
			public void run()
			{
				try {
					final URL url = new URL("http://graph.facebook.com/" + fuid + "/picture?type=square");
					Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
					final BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(bitmap);
					Handler mainHandler = new Handler(context.getMainLooper());
					mainHandler.post(new Runnable() {
						public void run() {
							try {
								marker.setIcon(icon);
							}
							catch (Exception e) {
								e.printStackTrace();
							}
						};
					});
				}
				catch (Exception e) {
					e.printStackTrace();
					FacebookPictureMarker.this.fuid = null;
				}
			};
		}.start();
		
	}

	public Marker getMarker() {
		return marker;
	}
}
