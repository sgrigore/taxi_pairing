package ro.pub.cs.taxi.android;


import java.util.HashSet;
import java.util.Set;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public abstract class SettingsManager implements NotificationSettings {

	private Set<String> data;

	public SettingsManager() {
		data = new HashSet<String>();
	}

	protected String getPrefName() {
		return BaseApplication.getInstance().getTitle() + "_settings";
	}

	protected void registerSetting(String name) {
		data.add(name);
	}

	public void save() {

		final String PREFS_NAME = getPrefName();
		SharedPreferences settings = BaseApplication.getInstance().getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();

		for (String key : data) {
			saveObject(key, editor);
		}

		editor.commit();
	}

	public void restore() {

		final String PREFS_NAME = getPrefName();
		SharedPreferences settings = BaseApplication.getInstance().getSharedPreferences(PREFS_NAME, 0);

		for (String key : data) {
			restoreObject(key, settings);
		}
	}

	protected abstract boolean restoreObject(String key, SharedPreferences settings);
	protected abstract boolean saveObject(String key, Editor editor);

}