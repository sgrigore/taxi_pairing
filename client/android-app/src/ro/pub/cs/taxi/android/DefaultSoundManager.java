package ro.pub.cs.taxi.android;

import android.content.Context;

public class DefaultSoundManager extends SoundManager {

	private boolean en = false;
	public DefaultSoundManager(Context context) {
		super(context);
	}

	@Override
	protected int getNewIndex(int oldIndex) {
		return -1;
	}
	
	public void resetIndex() {
		this.songIndex = -1;
	}

	@Override
	protected int getSongId(int index) {
		return -1;
	}

	@Override
	public boolean isEnabled() {
		return this.en ;
	}
	
	public void setEnabled(boolean b) {
		this.en = b;
	}

}