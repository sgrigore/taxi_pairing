package ro.pub.cs.taxi.android;

import ro.pub.cs.taxi.android.Quality.QualitySettings;
import ro.pub.cs.taxi.android.SoundManager.SoundSettings;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class DefaultSettingsManager extends SettingsManager implements QualitySettings, SoundSettings {

	private boolean sound;
	private boolean sfx;
	private boolean vibration;
	private int quality;

	public static final int DEF_QUALITY = 0;
	public static final boolean DEF_SOUND = true;
	public static final boolean DEF_SFX = true;
	public static final boolean DEF_VIBRATE = true;

	public DefaultSettingsManager() {
		super();
		registerSetting("quality");
		registerSetting("sound");
		registerSetting("sfx");
		registerSetting("vibration");
	}

	@Override
	protected boolean restoreObject(String key, SharedPreferences settings) {
		if (key.equals("quality")) {
			setQuality(settings.getInt(key, DEF_QUALITY));
			return true;
		} else if (key.equals("sfx")) {
			setSFX(settings.getBoolean(key, DEF_SFX));
			return true;
		} else if (key.equals("sound")) {
			setSound(settings.getBoolean(key, DEF_SOUND));
			return true;
		} else if (key.equals("vibration")) {
			setVibrate(settings.getBoolean(key, DEF_VIBRATE));
			return true;
		}
		return false;
	}

	@Override
	protected boolean saveObject(String key, Editor editor) {
		if (key.equals("quality")) {
			editor.putInt(key, getQuality());
			return true;
		} else if (key.equals("sfx")) {
			editor.putBoolean(key, getSFX());
			return true;
		} else if (key.equals("sound")) {
			editor.putBoolean(key, getSound());
			return true;
		} else if (key.equals("vibration")) {
			editor.putBoolean(key, getVibrate());
			return true;
		}
		return false;
	}

	@Override
	public boolean getSound() {
		return sound;
	}

	@Override
	public void setSound(boolean sound) {
		// no change
		if (this.sound == sound) {
			return;
		}
		this.sound = sound;
		SoundManager sm = BaseApplication.getInstance().getSoundManager();
		if (sound == false) {
			sm.stopMusic();
		} else {
			sm.playNewMusic();
		}
	}

	@Override
	public boolean getSFX() {
		return sfx;
	}

	@Override
	public void setSFX(boolean sfx) {
		this.sfx = sfx;
	}

	@Override
	public boolean getVibrate() {
		return vibration;
	}

	@Override
	public void setVibrate(boolean vibration) {
		this.vibration = vibration;
	}

	@Override
	public int getQuality() {
		return quality;
	}

	@Override
	public void setQuality(int q) {
		this.quality = q;
	}

	@Override
	public boolean getUseNotify() {
		return true;
	}

	@Override
	public void setUseNotify(boolean value) {
	}

}