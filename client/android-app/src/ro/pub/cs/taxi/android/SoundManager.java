package ro.pub.cs.taxi.android;

import ro.pub.cs.taxi.R;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.SoundPool;
import android.os.Vibrator;

public abstract class SoundManager {

	public static interface SoundSettings {
		boolean getSound();
		void setSound(boolean sound);
		boolean getSFX();
		void setSFX(boolean sfx);
		boolean getVibrate();
		void setVibrate(boolean vibration);

	}

	protected MediaPlayer mp;

	protected Vibrator v;
	protected SoundPool sounds;
	int songIndex;

	private int click;

	protected SoundManager(Context context) {
		initSound(context);
		songIndex = getNewIndex(-1);
	}

	protected SoundSettings getSettings() {
		try {
			SettingsManager sm = BaseApplication.getInstance().getSettingsManager();
			SoundSettings qs = (SoundSettings)sm;
			return qs;
		}
		catch (ClassCastException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void initSound(Context context) {
		songIndex = -1;
		stopMusic();
		sounds = new SoundPool(16, AudioManager.STREAM_MUSIC, 0);
		v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);

		click = sounds.load(context, R.raw.click_s, 0);
	}

	protected SoundPool getSoundPool() {
		return sounds;
	}

	protected final void playSFX(int sound_id) {
		playSFX(sound_id, 0.8f);
	}

	protected final  void playSFX(int sound_id, float volume) {
		if (!getSettings().getSFX())
			return;
		if (sounds != null)
			sounds.play(sound_id, volume, volume, 1, 0, 1);
	}

	public void stopMusic() {

		if (mp == null)
			return;

		try{
			mp.stop();
			mp.release();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		mp = null;
	}
	
	public void resumeMusic() {
		if (isEnabled() == false)
			return;
		if (mp == null)
			playNewMusic();
	}

	public void playMusic() {
		if (isEnabled() == false)
			return;
		playNewMusic();
	}

	abstract protected int getNewIndex(int oldIndex);
	abstract protected int getSongId(int index);
	public abstract boolean isEnabled();

	public void playNewMusic() {

		if (isEnabled() == false)
			return;
		if(songIndex == getNewIndex(songIndex) )
        	return;
		stopMusic();

		if (!getSettings().getSound())
			return;
        
		songIndex = getNewIndex(songIndex);
		final int music_id = getSongId(songIndex);
		try {
			mp = null;
			if (music_id == -1)
				return;
			mp = MediaPlayer.create(BaseApplication.getInstance(), music_id);
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		if (mp == null)
			return;

		mp.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				mp.release();
				songIndex = -1;
				playNewMusic();
			}
		});
		mp.start();
	}

	public final void release() {
		sounds.release();
		sounds = null;
	}

	public void playClick() {
		if(getSettings().getVibrate() && v != null)
			v.vibrate(20);
		playSFX(click, 0.2f);
	}
}