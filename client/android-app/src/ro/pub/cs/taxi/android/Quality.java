package ro.pub.cs.taxi.android;

public class Quality {

	public static interface QualitySettings {
		int getQuality();
		void setQuality(int q);
	}

	protected static final int q() {
		try {
			SettingsManager sm = BaseApplication.getInstance().getSettingsManager();
			QualitySettings qs = (QualitySettings)sm;
			return qs.getQuality();
		}
		catch (ClassCastException e) {
			e.printStackTrace();
		}
		return 0;
	}

}