package ro.pub.cs.taxi.android;

import java.util.HashMap;
import java.util.Map;

import android.app.Application;
import android.content.pm.PackageManager.NameNotFoundException;

public abstract class BaseApplication extends Application {

	static class DataStructure {
		public SettingsManager settingsManager = null;
		public SoundManager soundManager = null;
	};

	private static BaseApplication instance = null;

	private DataStructure data;
	private HashMap<String, Object> hash;

	@Override
	public void onCreate() {
		super.onCreate();
		instance = this;
		hash = new HashMap<String, Object>();
		data.soundManager = newSoundManager();
		data.settingsManager = newSettingsManager();
		data.settingsManager.restore();
	}

	public BaseApplication() {
		data = new DataStructure();
	}

	public static BaseApplication getInstance() {
		return instance;
	}

	public String getTitle() {
		return getApplicationContext().getPackageName();
	}

	public String getVersion() {
		try {
			return getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
		} catch (NameNotFoundException e) {
			return "";
		}
	}

	final public SettingsManager getSettingsManager() {
		return data.settingsManager;
	}

	final public DefaultSoundManager getSoundManager() {
		return (DefaultSoundManager)(data.soundManager);
	}

	abstract protected SoundManager newSoundManager();
	abstract protected SettingsManager newSettingsManager();

	public Map<String, Object> getHash() {
		return hash;
	}
}
