package ro.pub.cs.taxi.android;

public interface NotificationSettings {
	boolean getUseNotify();
	void setUseNotify(boolean value);
}