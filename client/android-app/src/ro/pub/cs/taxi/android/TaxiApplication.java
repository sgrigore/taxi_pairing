package ro.pub.cs.taxi.android;

public class TaxiApplication extends BaseApplication {

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	protected SoundManager newSoundManager() {
		return new DefaultSoundManager(this);
	}

	@Override
	protected SettingsManager newSettingsManager() {
		return new DefaultSettingsManager();
	}

}
