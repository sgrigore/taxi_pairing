package ro.pub.cs.taxi.driver;

import ro.pub.cs.taxi.server.Coords;
import ro.pub.cs.taxi.server.driver.DecideRequest;

public class Request {

	private String mId;
	private Coords mSource;
	private Coords mDestination;
	private DecideRequest mDecideRequest;
	
	public Request(String id, Coords src, Coords dst, DecideRequest r) {
		mId	= id;
		mSource = src;
		mDestination = dst;
		mDecideRequest = r;
	}

	public String getId() {
		return mId;
	}

	public Coords getSource() {
		return mSource;
	}
	
	public Coords getDestination() {
		return mDestination;
	}
	
	public DecideRequest getDecideRequest() {
		return mDecideRequest;
	}
}
