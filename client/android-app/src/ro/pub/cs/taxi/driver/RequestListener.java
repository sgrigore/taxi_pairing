package ro.pub.cs.taxi.driver;

import ro.pub.cs.taxi.server.Coords;
import ro.pub.cs.taxi.server.driver.DecideRequest;

public interface RequestListener {
	void onRequestOrder(String rid, Coords src, Coords dst, DecideRequest r);
	void onOrderConfirmation(boolean confirmed);
	void onLoginDone(boolean result, String reason);
}
