package ro.pub.cs.taxi.driver;

import java.util.HashMap;

import ro.pub.cs.taxi.R;
import ro.pub.cs.taxi.android.TaxiApplication;
import ro.pub.cs.taxi.driver.location.LiniarMovementLocationProvider;
import ro.pub.cs.taxi.driver.location.LocationChangeListener;
import ro.pub.cs.taxi.driver.location.LocationProvider;
import ro.pub.cs.taxi.driver.location.LocationResolver;
import ro.pub.cs.taxi.server.Coords;
import ro.pub.cs.taxi.server.IPResolver;
import ro.pub.cs.taxi.server.driver.DecideRequest;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

public class DriverActivity extends FragmentActivity implements OnInfoWindowClickListener, 
					LocationChangeListener, RequestListener, OnMarkerClickListener {
	
	public final static String TAG = "DriverActivity";

	private int mState;
	private final static int STATE_INITIAL = 0;
	private final static int STATE_ACCEPTING_REQUESTS = 1;
	private final static int STATE_REQUEST_PENDING = 2;
	private final static int STATE_MOVING_TO_CLIENT = 3;
	
	private LocationProvider mLocationProvider;
	private GoogleMap mMap;
	private Location mMyLocation;
	private Marker mMyLocationMarker;
	private Marker mDestinationMarker;
	
	private DriverActivity mActivity;
	private RequestManager mRequestManager;
	
	private HashMap<String, Marker> mMarkers;
	private HashMap<Marker, String> mMarkerMap;
	private HashMap<String, Request> mRequests;
	private String mCurrentRid;
	
	private IPResolver mIPResolver;
	private LocationResolver mLocationResolver;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_driver_main);
		mActivity = this;
		mState = STATE_INITIAL;
		mCurrentRid = null;
		
		mIPResolver = new IPResolver();
		mLocationResolver = new LocationResolver(this);
		
		mMarkers = new HashMap<String, Marker>();
		mMarkerMap = new HashMap<Marker, String>();
		mRequests = new HashMap<String, Request>();
		
		// get map object
		mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
	    mMyLocationMarker = null;
	    mMyLocation = null;
	    mDestinationMarker = null;
		
	    //mLocationProvider = new DefaultLocationProvider(this);
	    mLocationProvider = new LiniarMovementLocationProvider(this, 44.43534, 26.10210, 0);
	    
	    if (mLocationProvider.shouldBeUsedWithMyLocationOnMap()) {
		    mMap.setMyLocationEnabled(true);
	    }
	    
	    Location location = mLocationProvider.getLastKnownLocation();
	    // Initialize the location fields
	    if (location != null) {
	        CameraUpdate update = CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),
	                					location.getLongitude()));
	        mMap.moveCamera(update);
	        mMap.animateCamera(CameraUpdateFactory.zoomTo(14));
	    	onLocationChanged(location);
	    }
	    
	    mLocationProvider.setLocationChangeListener(this);
	    mMap.setOnInfoWindowClickListener(this);
	    mMap.setOnMarkerClickListener(this);
	    
	    try {
	    	mRequestManager = new RequestManager(this, mIPResolver, 3333);
		} catch (Exception e) {
			e.printStackTrace();
			showToast("Error connecting to server");
		}

	    showToast("Connecting to server ...");
	    final String name = getIntent().getExtras().getString("name");
	    final String pass = getIntent().getExtras().getString("pass");
	    new Thread(new Runnable(){
			@Override
			public void run() {
			    mRequestManager.login(name, pass);	
			}
	    }).start();
	}

	@Override
	protected void onResume() {
		super.onResume();
		mLocationProvider.resume();
	}

	@Override
	protected void onPause() {
		super.onPause();
		mLocationProvider.pause();
	}

	@Override
	public void onLocationChanged(Location location) {
		if (mState == STATE_INITIAL && mMyLocation == null && location != null) {
			mState = STATE_ACCEPTING_REQUESTS;
		}
		mMyLocation = location;
		
		if (mMyLocationMarker != null) {
			mMyLocationMarker.remove();
		}
		
	    mMyLocationMarker = mMap.addMarker(new MarkerOptions()
	        .position(new LatLng(location.getLatitude(), location.getLongitude()))
	        .title("You")
	        .snippet("You are here")
	        .icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi_selected))
	    );
	    
	    if (mRequestManager != null)
	    	mRequestManager.updateLocation(new Coords(location.getLatitude(), location.getLongitude(), location.getAltitude()));
	}

	@Override
	public void onDestinationReached() {
		showToast("Destination reached");
	}
	
	public void showToast(final String msg) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
	   			Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	public void onRequestOrder(String rid, Coords src, Coords dst,
			DecideRequest r) {
		if (mState != STATE_ACCEPTING_REQUESTS && mState != STATE_REQUEST_PENDING) {
			r.reject();
			return;
		}
		
		final Request req = new Request(rid, src, dst, r);
		final LatLng ll = new LatLng(src.getLatitude(), src.getLongitude());
		mRequests.put(rid, req);
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Marker m = mMap.addMarker(new MarkerOptions()
	        				.position(ll)
	        				.title("Id: " + req.getId())
	        				.icon(BitmapDescriptorFactory.fromResource(R.drawable.person)));
				mMarkers.put(req.getId(), m);
				mMarkerMap.put(m, req.getId());
				mLocationResolver.setMarkerAddress(ll.latitude, ll.longitude, m);
				TaxiApplication taxiapp = (TaxiApplication) mActivity.getApplicationContext();
				taxiapp.getSoundManager().playClick();
			}	
		});
	}

	@Override
	public void onOrderConfirmation(boolean confirmed) {
		if (mCurrentRid == null || mState != STATE_REQUEST_PENDING) {
			return;
		}
		
		if (!confirmed) {
			mCurrentRid = null;
			showToast("To slow ...");
			mState = STATE_ACCEPTING_REQUESTS;
			return;
		}
		
		Request r = mRequests.get(mCurrentRid);
		final Marker m = mMarkers.get(mCurrentRid);
		for (Request rq: mRequests.values()) {
			if (rq.getId().equals(mCurrentRid))
				continue;
			rq.getDecideRequest().reject();
		}
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				for (Marker mr: mMarkerMap.keySet()) {
					if (mr.equals(m))
						continue;
					mr.remove();
				}
			}
		});
		
		mMarkerMap.clear();
		mMarkers.clear();
		mRequests.clear();
		
		mState = STATE_MOVING_TO_CLIENT;
		Coords c = r.getSource();
		mLocationProvider.setDestination(c.getLatitude(), c.getLongitude(), c.getAltitude());
	}

	@Override
	public void onInfoWindowClick(Marker m) {
		if (m.equals(mMyLocationMarker)) {
			return;
		}
		if (mState == STATE_ACCEPTING_REQUESTS) {
			mState = STATE_REQUEST_PENDING;
			showToast("Waiting for confirmation ...");
			mCurrentRid = mMarkerMap.get(m);
			mRequests.get(mCurrentRid).getDecideRequest().accept();
		}
	}

	@Override
	public void onLoginDone(boolean result, String reason) {
		if (!result) {
			showToast("Error: " + reason);
			Intent i = new Intent(this, LoginActivity.class);
			startActivity(i);
			finish();
		}
		else {
			showToast("Successfully connected to server");
			Location l = mLocationProvider.getLastKnownLocation();
			mRequestManager.updateLocation(new Coords(l.getLatitude(), l.getLongitude(), l.getAltitude()));
		}
	}

	@Override
	public boolean onMarkerClick(Marker m) {
		String id = mMarkerMap.get(m);
		if (id == null)
			return false;
		Coords dest = mRequests.get(id).getDestination();
		Coords src = mRequests.get(id).getSource();
		
		if (dest != null && dest.getDistance(src.getLatitude() , src.getLongitude(), src.getAltitude()) < 40) {
			dest = null;
		}

		if (mDestinationMarker != null) {
			mDestinationMarker.remove();
			mDestinationMarker = null;
		}
		if (dest != null) {
			mDestinationMarker = mMap.addMarker(new MarkerOptions()
        				.position(new LatLng(dest.getLatitude(), dest.getLongitude()))
        				.title("Destination")
        				.icon(BitmapDescriptorFactory.fromResource(R.drawable.treasure)));
			mLocationResolver.setMarkerAddress(dest.getLatitude(), dest.getLongitude(), mDestinationMarker);
		}
		
		return false;
	}
}
