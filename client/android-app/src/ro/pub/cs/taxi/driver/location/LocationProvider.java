package ro.pub.cs.taxi.driver.location;

import android.location.Location;

public interface LocationProvider {
	void pause();
	void resume();
	Location getLastKnownLocation();
	void setLocationChangeListener(LocationChangeListener l);
	public void setDestination(double latitude, double longitude, double altitude);
	boolean shouldBeUsedWithMyLocationOnMap();
}
