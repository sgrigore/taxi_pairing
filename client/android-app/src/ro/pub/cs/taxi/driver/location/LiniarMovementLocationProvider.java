package ro.pub.cs.taxi.driver.location;

import ro.pub.cs.taxi.server.Coords;

import android.app.Activity;
import android.location.Location;

public class LiniarMovementLocationProvider implements LocationProvider, Runnable {

	private static final int MOVEMENT_STEP = 10;
	private static final int CHANGE_INTERVAL_MILLIS = 5000;
	private static final String mLocationProvider = "LinearMovementLocationProvider";
	
	private Activity mActivity;
	private Thread mThread;
	private volatile boolean mPaused;
	private LocationChangeListener mLocationChangeListener;
	private Coords mInitialCoords, mDestinationCoords;
	
	public LiniarMovementLocationProvider(Activity a, double latitude, double longitude, double altitude) {
		mPaused = false;
		mInitialCoords = new Coords(latitude, longitude, altitude);
		mDestinationCoords = null;
		mLocationChangeListener = null;
		
		mActivity = a;
		mThread = new Thread(this);
		mThread.start();
	}
	
	@Override
	public void pause() {
		mPaused = true;
		mThread = null;
	}

	@Override
	public void resume() {
		mPaused = false;
		
		mThread = new Thread(this);
		mThread.start();
	}

	@Override
	public Location getLastKnownLocation() {
		Location ret = new Location(mLocationProvider);
		ret.setLatitude(mInitialCoords.getLatitude());
		ret.setLongitude(mInitialCoords.getLongitude());
		ret.setAltitude(mInitialCoords.getAltitude());
		return ret;
	}

	@Override
	public void setLocationChangeListener(LocationChangeListener l) {
		mLocationChangeListener = l;
	}

	@Override
	public void setDestination(double latitude, double longitude, double altitude) {
		mDestinationCoords = new Coords(latitude, longitude, altitude);
	}

	@Override
	public void run() {
		Location currentLocation = getLastKnownLocation();
		while (!mPaused) {
			
			try {
				Thread.sleep(CHANGE_INTERVAL_MILLIS);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			if (mLocationChangeListener == null)
				continue;
			if (mDestinationCoords != null) {
				// destination reached
				if (mDestinationCoords.getDistance(currentLocation.getLatitude(), currentLocation.getLongitude(),
						currentLocation.getAltitude()) < 50) {
					mLocationChangeListener.onDestinationReached();
					mDestinationCoords = null;
					continue;
				}

				currentLocation.setLatitude(currentLocation.getLatitude() - 
						(mInitialCoords.getLatitude() - mDestinationCoords.getLatitude())/(double)MOVEMENT_STEP);
				currentLocation.setLongitude(currentLocation.getLongitude() - 
						(mInitialCoords.getLongitude() - mDestinationCoords.getLongitude())/(double)MOVEMENT_STEP);
				currentLocation.setAltitude(currentLocation.getAltitude() - 
						(mInitialCoords.getAltitude() - mDestinationCoords.getAltitude())/(double)MOVEMENT_STEP);
			}
			final LocationChangeListener change = mLocationChangeListener;
			final Location location = new Location(currentLocation);
			mActivity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					change.onLocationChanged(location);
				}
			});
		}
		mInitialCoords = new Coords(
				currentLocation.getLatitude(),
				currentLocation.getLongitude(),
				currentLocation.getAltitude()
		);
	}

	@Override
	public boolean shouldBeUsedWithMyLocationOnMap() {
		return false;
	}

}
