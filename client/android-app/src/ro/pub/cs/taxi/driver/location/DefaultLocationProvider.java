package ro.pub.cs.taxi.driver.location;

import ro.pub.cs.taxi.server.Coords;
import android.app.Activity;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;

public class DefaultLocationProvider implements LocationProvider, LocationListener {

	private LocationManager mLocationManager;
	private String mLocationProvider;
	private LocationChangeListener mLocationChangeListener;
	private Coords mDestinationCoords;
	
	public DefaultLocationProvider(Activity a) {
		// Enable GPS
		mLocationManager = (LocationManager) a.getSystemService(Activity.LOCATION_SERVICE);
		if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
				!mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			a.startActivity(intent);
		}
		
	    Criteria criteria = new Criteria();
	    mLocationProvider = mLocationManager.getBestProvider(criteria, false);
	}
	
	@Override
	public void pause() {
		mLocationManager.removeUpdates(this);	
	}

	@Override
	public void resume() {
		mLocationManager.requestLocationUpdates(mLocationProvider, 400, 1, this);
	}

	@Override
	public void setLocationChangeListener(LocationChangeListener l) {
		mLocationChangeListener = l;
	}

	@Override
	public Location getLastKnownLocation() {
		return mLocationManager.getLastKnownLocation(mLocationProvider);
	}

	@Override
	public void onLocationChanged(Location location) {
		if (mLocationChangeListener != null) {
			if (mDestinationCoords != null) {
				if (mDestinationCoords.getDistance(location.getLatitude(), location.getLongitude(),
						location.getAltitude()) < 50) {
					mLocationChangeListener.onDestinationReached();
					mDestinationCoords = null;
				}
			}
			mLocationChangeListener.onLocationChanged(location);
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public void setDestination(double latitude, double longitude,double altitude) {
		mDestinationCoords = new Coords(latitude, longitude, altitude);
	}

	@Override
	public boolean shouldBeUsedWithMyLocationOnMap() {
		return true;
	}

}
