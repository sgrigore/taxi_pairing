package ro.pub.cs.taxi.driver;

import ro.pub.cs.taxi.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends Activity implements OnClickListener {
	
	private Activity mActivity;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_driver_login);
		
        Button button = (Button) findViewById(R.id.login_button);
        button.setOnClickListener(this);
        
        mActivity = this;
	}

	@Override
	public void onClick(View arg0) {
        EditText nameField = (EditText) findViewById(R.id.login_name);
        EditText passField = (EditText) findViewById(R.id.login_pass);
		String name = nameField.getText().toString();
		String pass = passField.getText().toString();
		
		if (name == null || name.trim().length() == 0 || 
			pass == null || pass.trim().length() == 0) {
			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(mActivity, "Fill in both fields!", Toast.LENGTH_SHORT).show();
				}
			});
			return;
		}
		
		Intent i = new Intent(this, DriverActivity.class);
		i.putExtra("name", name);
		i.putExtra("pass", pass);
		startActivity(i);
		finish();
	}
	
}
