package ro.pub.cs.taxi.driver.location;

import android.location.Location;

public interface LocationChangeListener {
	public void onLocationChanged(Location location);
	public void onDestinationReached();
}