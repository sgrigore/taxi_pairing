package ro.pub.cs.taxi.driver.location;

import java.io.IOException;
import java.util.List;
import com.google.android.gms.maps.model.Marker;

import android.location.Address;
import android.location.Geocoder;
import android.support.v4.app.FragmentActivity;

public class LocationResolver {

	public final static int MAX_WAIT_TIME = 5000;
	
	private Geocoder mGeocoder;
	private FragmentActivity mActivity;
	
	public LocationResolver(FragmentActivity activity) {
		mGeocoder = new Geocoder(activity);
		mActivity = activity;
	}
	
	public void setMarkerAddress(final double latitude, final double longitude, final Marker m) {
		class MyThread extends Thread {
			volatile String value;
			
			public MyThread() {
				value = null;
			}
			
			@Override
			public void run() {
				try {
					List<Address> l = mGeocoder.getFromLocation(latitude, longitude, 1);
					if ((l == null || l.size() == 0) == false) {
						Address a = l.get(0);
						String ret = a.getAddressLine(0);
						if (a.getAddressLine(1) != null)
							ret += ", " + a.getAddressLine(1);
						value = ret;
					}
				} catch (IOException e) {
				}
				
				final String snippet = value;
				
				if (snippet != null) {
					mActivity.runOnUiThread(new Runnable() {
						@Override
						public void run() {
							try {
								m.setSnippet(snippet);
							}
							catch (Exception e) {
								
							}
						}
					});
				}
			}
		}
			
		new MyThread().start();
	}
}
