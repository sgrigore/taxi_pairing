package ro.pub.cs.taxi.driver;

import ro.pub.cs.taxi.server.Coords;
import ro.pub.cs.taxi.server.IPResolver;
import ro.pub.cs.taxi.server.driver.DecideRequest;
import ro.pub.cs.taxi.server.driver.DriverStub;

public class RequestManager extends DriverStub {

	private RequestListener mRequestListener;
	
	public RequestManager(RequestListener l, IPResolver ip, int port) throws Exception {
		super(ip, port);
		mRequestListener = l;
	}
	
	@Override
	public void onServerOrder(String rid, Coords src, Coords dst, DecideRequest r) {
		mRequestListener.onRequestOrder(rid, src, dst, r);
	}

	@Override
	public void onOrderConfirmation(boolean confirmed) {
		mRequestListener.onOrderConfirmation(confirmed);
	}

	@Override
	public void onLoginDone(boolean result, String reason) {
		mRequestListener.onLoginDone(result, reason);
	}

}
