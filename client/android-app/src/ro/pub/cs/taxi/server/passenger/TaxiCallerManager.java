package ro.pub.cs.taxi.server.passenger;

import java.util.List;

import ro.pub.cs.taxi.server.Coords;
import ro.pub.cs.taxi.server.async.Continuation;

public interface TaxiCallerManager {
	
	TaxiCaller getTaxiCaller();

	void getNearbyTaxi(final Coords coords,
			final String facebook_token,
			final Continuation<List<TaxiRecommendation>> c);
	
	void makeRequest(final Coords src, final Coords dst, final Iterable<String> ids,
			final String key, final Continuation<String> c);
	
	void checkStatus(final String rid, final String key, final String facebook_id,
			final Continuation<TaxiStatus> c);
	
	void waitForReady(final String rid, final String key, final String facebook_id,
			final Continuation<TaxiStatus> c);
	
	void queryLocation(final TaxiStatus taxi, final Continuation<Coords> c);

	void addReview(final String reviewId, final String content,
			final Continuation<Boolean> c);
}
