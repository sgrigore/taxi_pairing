package ro.pub.cs.taxi.server.passenger;

import java.util.List;

import ro.pub.cs.taxi.server.Coords;
import ro.pub.cs.taxi.server.IPResolver;
import ro.pub.cs.taxi.server.async.Continuation;

public class TaxiCallerManagerImpl implements TaxiCallerManager {

	public static final int QUERY_INTERVAL = 5000;
	public static final int MAX_QUERY_RETRY = 30;
	
	private TaxiCaller caller;

	public TaxiCallerManagerImpl(IPResolver ip, int port) {
		this.caller = new TaxiCallerImpl(ip, port);
	}
	
	public TaxiCaller getTaxiCaller() {
		return caller;
	}

	@Override
	public void getNearbyTaxi(final Coords coords,
			final String facebook_token,
			final Continuation<List<TaxiRecommendation>> c)
	{
		new Thread() {
			@Override
			public void run() {
				try {
					List<TaxiRecommendation> result = caller.get_nearby_taxi(coords, facebook_token);
					c.actionFinished(true, result);
				} catch (Exception e) {
					e.printStackTrace();
					c.actionFinished(false, null);
				}
			}
		}.start();
	}

	@Override
	public void makeRequest(final Coords src, final Coords dst, final Iterable<String> ids,
			final String key, final Continuation<String> c)
	{
		new Thread() {
			@Override
			public void run() {
				try {
					String result = caller.make_request(src, dst, ids, key);
					c.actionFinished(true, result);
				} catch (Exception e) {
					c.actionFinished(false, null);
				}
			}
		}.start();
	}
	
	@Override
	public void checkStatus(final String rid, final String key, final String facebook_id,
			final Continuation<TaxiStatus> c)
	{
		new Thread() {
			@Override
			public void run() {
				try {
					TaxiStatus result = caller.check_status(rid, key, facebook_id);
					c.actionFinished(true, result);
				} catch (Exception e) {
					c.actionFinished(false, null);
				}
			}
		}.start();
	}
	

	@Override
	public void waitForReady(final String rid, final String key, 
			final String facebook_id, final Continuation<TaxiStatus> c)
	{
		new Thread() {
			@Override
			public void run() {
				for (int i = 0; i < MAX_QUERY_RETRY; i++) {
					try {
						TaxiStatus result = caller.check_status(rid, key, facebook_id);
						if (result.getStatus().equals("READY")) {
							c.actionFinished(true, result);
						}
						else if (result.getStatus().equals("NO_RESPONSE")) {
							c.actionFinished(false, null);
							return;
						}
						Thread.sleep(QUERY_INTERVAL);
					} catch (Exception e) {
						e.printStackTrace();
						c.actionFinished(false, null);
					}
				}
			}
		}.start();	
	}
	
	@Override
	public void queryLocation(final TaxiStatus taxi, final Continuation<Coords> c)
	{
		new Thread() {
			@Override
			public void run() {
				try {
					Coords result = caller.query_location(taxi);
					c.actionFinished(true, result);
				} catch (Exception e) {
					c.actionFinished(false, null);
				}
			}
		}.start();
	}

	@Override
	public void addReview(final String reviewId, final String content,
			final Continuation<Boolean> c)
	{
		new Thread() {
			@Override
			public void run() {
				try {
					Boolean result = caller.add_review(reviewId, content);
					c.actionFinished(result, result);
				} catch (Exception e) {
					c.actionFinished(false, false);
				}
			}
		}.start();
	}
}
