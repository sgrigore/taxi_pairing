package ro.pub.cs.taxi.server.driver;

public interface DecideRequest {

	void accept();
	void reject();
}
