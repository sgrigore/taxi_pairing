package ro.pub.cs.taxi.server.passenger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ro.pub.cs.taxi.server.Coords;
import ro.pub.cs.taxi.server.IPResolver;

class TaxiRecomandationImpl implements TaxiRecommendation {

	private static final long serialVersionUID = -3409009401561214897L;
	private final String id;
	private final String name;
	private final Coords coords;
	private final List<TaxiReview> list;

	public TaxiRecomandationImpl(JSONObject obj) throws JSONException {
		this.id = obj.getString("id");
		this.name = obj.getString("username");
		JSONObject jcoords = obj.getJSONObject("coords");
		this.coords = new Coords(jcoords.getDouble("lat"),
				jcoords.getDouble("long"),
				jcoords.getDouble("alt"));
		list = new ArrayList<TaxiReview>();
		final JSONArray jar = obj.getJSONArray("reviews");
		
		class TaxiReviewImpl implements TaxiReview {
			private static final long serialVersionUID = 2938631076514693343L;
			private String content;
			private String fuid;
			private String fname;
			public TaxiReviewImpl(String content, String fuid, String fname) {
				this.content = content;
				this.fuid = fuid;
				this.fname = fname;
			}
			@Override
			public String getFUID() {
				return fuid;
			}
			@Override
			public String getText() {
				return content;
			}
			@Override
			public String getName() {
				return fname;
			}
		};
		
		for (int i = 0; i < jar.length(); i++) {
			final String fuid = jar.getJSONObject(i).getString("fuid");
			final String content = jar.getJSONObject(i).getString("content");
			final String fname = jar.getJSONObject(i).getString("name");
			list.add(new TaxiReviewImpl(content, fuid, fname));
		}
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Coords getCoords() {
		return coords;
	}

	@Override
	public List<TaxiReview> getReviews() {
		return list;
	}
}

public class TaxiCallerImpl implements TaxiCaller {

	private int port;
	private IPResolver ip;
	private String mbaseurl;

	public TaxiCallerImpl(IPResolver ip, int port) {
		this.ip = ip;
		this.port = port;
	}

	public String getBaseURL() {
		if (mbaseurl != null) {
			return mbaseurl;
		}
		mbaseurl = "http://" + ip.getIPBlocking() + ":" + port;
		return mbaseurl;
	}

	public static final String readFully(final InputStream is) throws IOException {
		final int IO_BUFFER_SIZE = 128;
		final StringWriter rval = new StringWriter();
		final char buff[] = new char[IO_BUFFER_SIZE];
		final Reader read = new BufferedReader(new InputStreamReader(is, "UTF-8"));
		int cnt = 0;
		try {
			for (;;) {
				cnt = read.read(buff);
				if (cnt != -1) {
					rval.write(buff, 0, cnt);
					continue;
				}
				break;
			}
		}
		finally {
			try {
				is.close();
			}
			catch(Exception e) {
			}
		}

		return rval.toString();
	}

	private String getREST(String call) throws IOException {
		final HttpClient httpClient = new DefaultHttpClient();
		final HttpGet httpGet = new HttpGet(getBaseURL() + call);
		final HttpResponse httpResponse = httpClient.execute(httpGet);
		final int statusCode = httpResponse.getStatusLine().getStatusCode();
		if (statusCode != HttpStatus.SC_OK)
		{
			return null;
		}

		String str = readFully(httpResponse.getEntity().getContent());
		android.util.Log.d("my", str);
		return str;
	}
	
	private String postREST(String call, List<BasicNameValuePair> namedValuePairs)
			throws IOException {
		final HttpClient httpClient = new DefaultHttpClient();
		final HttpPost httpPost = new HttpPost(getBaseURL() + call);
		httpPost.setEntity(new UrlEncodedFormEntity(namedValuePairs));
		final HttpResponse httpResponse = httpClient.execute(httpPost);
		final int statusCode = httpResponse.getStatusLine().getStatusCode();
		if (statusCode != HttpStatus.SC_OK)
		{
			return null;
		}

		return readFully(httpResponse.getEntity().getContent());
	}

	@Override
	public List<TaxiRecommendation> get_nearby_taxi(Coords coords,
			String facebook_token) throws IOException, JSONException {
		if (facebook_token == null) {
			facebook_token = "";
		}
		else {
			facebook_token = "/" + facebook_token;
		}
		final String response = getREST("/client/get_nearby_taxi/" + coords + facebook_token);
		final JSONArray json = new JSONArray(response);

		final List<TaxiRecommendation> list = new ArrayList<TaxiRecommendation>();
		for (int i = 0; i < json.length(); i++) {
			JSONObject jobj = json.getJSONObject(i);
			list.add(new TaxiRecomandationImpl(jobj));
		}

		return list;
	}

	@Override
	public String make_request(Coords src, Coords dst, Iterable<String> ids,
			String key) throws IOException, JSONException {
		final StringWriter wr = new StringWriter();
		wr.write("/client/make_request/");
		wr.write(src.toString() + "/");
		if (dst != null) {
			wr.write(dst.toString() + "/");
		}
		wr.write("taxi/");
		for (String id : ids) {
			wr.write(id);
			wr.write("/");
		}
		wr.write(key);
		final String response = getREST(wr.toString());
		final JSONObject json = new JSONObject(response);
		return json.getString("rid");
	}

	@Override
	public TaxiStatus check_status(String rid, String key, String facebook_id)
			throws IOException, JSONException {
		if (facebook_id == null) {
			facebook_id = "";
		}
		else {
			facebook_id = "/" + facebook_id;
		}
		final String response = getREST("/client/check_status/" + rid + "/" + key + facebook_id);
		final JSONObject json = new JSONObject(response);
		final String status = json.getString("status");
		final String loc;
		final String id;
		final String regNumber;
		final String reviewID;
		if (status.equals("READY")) {
			loc = json.getString("locationKey");
			id = json.getString("taxiId");
			regNumber = json.getString("regNumber");
			reviewID = json.getString("reviewId");
		}
		else {
			loc = null;
			id = null;
			regNumber = null;
			reviewID = null;
		}
		return new TaxiStatus() {
			@Override
			public String getLocationKey() {
				return loc;
			}
			@Override
			public String getID() {
				return id;
			}
			@Override
			public String getRegNumber() {
				return regNumber;
			}
			@Override
			public String getStatus() {
				return status;
			}
			@Override
			public boolean isReady() {
				return status.equals("READY");
			}
			@Override
			public String getReviewID() {
				return reviewID;
			}
		};
	}

	@Override
	public Coords query_location(TaxiStatus taxi)
			throws IOException, JSONException {
		final String response = getREST("/client/query_location/" + taxi.getLocationKey());
		final JSONObject json = new JSONObject(response);
		if (json.getString("status").equals("OK")) {
			JSONObject jcoords = json.getJSONObject("coords");
			return new Coords(jcoords.getDouble("lat"),
					jcoords.getDouble("long"),
					jcoords.getDouble("alt"));
		}
		return null;
	}

	@Override
	public boolean add_review(String review_id, String content)
			throws IOException, JSONException {
		final List<BasicNameValuePair> named = new ArrayList<BasicNameValuePair>();
		named.add(new BasicNameValuePair("content", content));
		final String response = postREST("/client/add_review/" + review_id, named);
		final JSONObject json = new JSONObject(response);
		return json.getString("status").equals("OK");
	}
}
