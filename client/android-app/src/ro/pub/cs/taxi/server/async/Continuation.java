package ro.pub.cs.taxi.server.async;

public interface Continuation<T> {

	void actionFinished(boolean success, T result);

}
