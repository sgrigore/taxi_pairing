package ro.pub.cs.taxi.server.driver;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.json.JSONObject;

import ro.pub.cs.taxi.server.Coords;
import ro.pub.cs.taxi.server.IPResolver;

public class DriverStub extends Thread {

	private Socket socket;
	private IPResolver ip;
	private int port;

	public DriverStub(IPResolver ip, int port) {
		this.ip = ip;
		this.port = port;
	}

	public void login(String username, String pass) {
		try {
			this.socket = new Socket(ip.getIPBlocking(), port);
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		if (socket == null) {
			onLoginDone(false, "Connection to server failed");
			return;
		}
		start();
		try {
			JSONObject json = new JSONObject();
			json.put("type", "login");
			json.put("username", username);
			json.put("password", pass);
			writeJSON(json);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onLoginDone(boolean result, String reason) {
	}

	public void onServerOrder(String rid, Coords src, Coords dst, DecideRequest r) {
		r.reject();
	}

	public void onOrderConfirmation(boolean confirmed) {
	}

	public void updateLocation(Coords coords) {
		try {
			JSONObject jobj = new JSONObject();
			jobj.put("type", "update_location");
			jobj.put("coords", coords.toJSON());
			writeJSON(jobj);
		}
		catch (Exception e) {
		}
	}

	@Override
	public void run() {
		while (true) {
			try {
				JSONObject jobj = readJSON();
				String type = jobj.getString("type");

				if (type.equals("login_ack")) {
					onLoginDone(true, null);
				}
				else if (type.equals("login_nack")) {
					onLoginDone(false, jobj.getString("reason"));
				}
				else if (type.equals("serve_order")) {
					final String rid = jobj.getString("rid");
					JSONObject jcoords = jobj.getJSONObject("src");
					Coords src = new Coords(jcoords.getDouble("lat"),
							jcoords.getDouble("long"),
							jcoords.getDouble("alt"));
					jcoords = jobj.getJSONObject("src");
					Coords dest = new Coords(jcoords.getDouble("lat"),
							jcoords.getDouble("long"),
							jcoords.getDouble("alt"));
					class DecideRequestImp implements DecideRequest, Runnable {
						boolean accepted = false;
						Thread thread = null;
						public DecideRequestImp() {
							thread = new Thread(this);
						}
						@Override
						public void run() {
							try {
								JSONObject reply = new JSONObject();
								reply.put("type", "serve_order");
								reply.put("rid", rid);
								reply.put("response", accepted ? "accept" : "reject");
								writeJSON(reply);
							} catch (Exception e) {
								e.printStackTrace();
							}
							finally {
								thread = null;
							}
						}
						@Override
						public void accept() {
							this.accepted = true;
							thread.start();
						}
						@Override
						public void reject() {
							this.accepted = false;
							thread.start();
						}
					};
					onServerOrder(rid, src, dest, new DecideRequestImp());
				}
				else if (type.equals("order_confirmation")) {
					onOrderConfirmation(jobj.getString("response").equals("accept"));
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private JSONObject readJSON() throws Exception {
		int size = readInt();
		byte bytes[] = new byte[size];
		socket.getInputStream().read(bytes);
		String str = new String(bytes, "UTF-8");
		JSONObject rval = new JSONObject(str);
		return rval;
	}

	private void writeJSON(JSONObject json) throws Exception {
		byte[] str = json.toString().getBytes();
		int len = str.length;
		writeInt(len);
		socket.getOutputStream().write(str);
	}

	private int readInt() throws IOException {
		byte b[] = new byte[4];
		int r = socket.getInputStream().read(b);
		if (r != b.length) {
			throw new IOException("Bye");
		}
		ByteBuffer bb = ByteBuffer.wrap(b);
		bb.order(ByteOrder.BIG_ENDIAN);
		return bb.getInt();
	}

	private void writeInt(int i) throws IOException {
		byte b[] = new byte[4];
		ByteBuffer bb = ByteBuffer.wrap(b);
		bb.order(ByteOrder.BIG_ENDIAN);
		bb.asIntBuffer().put(i);
		socket.getOutputStream().write(b);
	}
}
