package ro.pub.cs.taxi.server.passenger;

import java.io.IOException;
import java.util.List;

import org.json.JSONException;

import ro.pub.cs.taxi.server.Coords;

public interface TaxiCaller {

	List<TaxiRecommendation> get_nearby_taxi(Coords coords, String facebook_token) throws IOException, JSONException;

	String make_request(Coords src, Coords dst, Iterable<String> ids, String key) throws IOException, JSONException;

	TaxiStatus check_status(String rid, String key, String facebook_id) throws IOException, JSONException;

	Coords query_location(TaxiStatus taxi) throws IOException, JSONException;

	boolean add_review(String review_id, String content) throws IOException, JSONException;
}
