package ro.pub.cs.taxi.server.passenger;

public interface TaxiStatus {

	String getLocationKey();
	String getID();
	String getRegNumber();
	String getStatus();
	String getReviewID();
	boolean isReady();

}
