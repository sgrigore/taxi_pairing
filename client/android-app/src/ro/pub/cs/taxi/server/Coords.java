package ro.pub.cs.taxi.server;

import java.io.Serializable;

import org.json.JSONException;
import org.json.JSONObject;

public class Coords implements Serializable {

	private static final long serialVersionUID = 15955005780087877L;
	private double latitude;
	private double longitude;
	private double altitude;

	public Coords(double latitude, double longitude, double altitude) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.altitude = altitude;
	}

	@Override
	public String toString() {
		return latitude + ":" + longitude + ":" + altitude;
	}

	public JSONObject toJSON() throws JSONException {
		JSONObject obj = new JSONObject();
		obj.put("lat", latitude);
		obj.put("long", longitude);
		obj.put("alt", altitude);
		return obj;
	}
	
	public double getLatitude() {
		return latitude;
	}
	
	public double getLongitude() {
		return longitude;
	}

	public double getAltitude() {
		return altitude;
	}
	
	// TESTME
	public double getDistance(double lat, double lng, double alt) {
		double dLat = Math.toRadians(lat-latitude);
	    double dLon = Math.toRadians(lng-longitude);
	    double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	    Math.cos(Math.toRadians(latitude)) * Math.cos(Math.toRadians(lng)) *
	    Math.sin(dLon/2) * Math.sin(dLon/2);
	    double c = 2 * Math.asin(Math.sqrt(a));
	    return 6366000 * c;
	}
}
