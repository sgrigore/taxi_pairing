package ro.pub.cs.taxi.server.passenger;

import java.io.Serializable;
import java.util.List;

import ro.pub.cs.taxi.server.Coords;

public interface TaxiRecommendation extends Serializable {

	String getId();
	String getName();
	Coords getCoords();
	List<TaxiReview> getReviews();
}
