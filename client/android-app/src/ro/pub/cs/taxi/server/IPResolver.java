package ro.pub.cs.taxi.server;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import ro.pub.cs.taxi.config.TaxiConstants;
import ro.pub.cs.taxi.server.passenger.TaxiCallerImpl;

public class IPResolver {

	boolean failed = false;
	static String ip = null;
	static final String FAKE_DNS = "http://disturbed.grid.pub.ro/fake-dns.txt";
	
	public IPResolver() {
		if (ip != null)
		{
			return;
		}
		new Thread() {
			@Override
			public void run() {
				try {
					resolve();
				}
				catch (Exception e) {
					failed = true;
				}
			}
		}.start();
	}

	private void resolve() throws Exception {
		ip = null;
		final HttpClient httpClient = new DefaultHttpClient();
		final HttpGet httpGet = new HttpGet(FAKE_DNS);
		final HttpResponse httpResponse = httpClient.execute(httpGet);
		final int statusCode = httpResponse.getStatusLine().getStatusCode();
		if (statusCode != HttpStatus.SC_OK)
		{
			return;
		}
		
		synchronized (this) {
			ip = TaxiCallerImpl.readFully(httpResponse.getEntity().getContent());
			this.notifyAll();
		}
	}

	public String getIPBlocking() {
		if (true) return TaxiConstants.SERVER_IP;
		String ip = _getIPBlocking().trim();
		return ip;
	}
	
	private String _getIPBlocking() {
		if (ip != null) {
			return ip;
		}
		else if (failed) {
			return TaxiConstants.SERVER_IP;
		}
		synchronized (this) {
			try {
				if (ip != null)
				{
					return ip;
				}
				else if (failed)
				{
					return TaxiConstants.SERVER_IP;
				}
				else
				{
					this.wait();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return TaxiConstants.SERVER_IP;
	}

}
