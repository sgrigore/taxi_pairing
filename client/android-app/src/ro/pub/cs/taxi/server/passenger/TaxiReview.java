package ro.pub.cs.taxi.server.passenger;

import java.io.Serializable;

public interface TaxiReview extends Serializable {

	String getFUID();
	String getName();
	String getText();
}
