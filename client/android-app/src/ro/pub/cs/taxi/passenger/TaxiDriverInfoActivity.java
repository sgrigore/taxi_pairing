package ro.pub.cs.taxi.passenger;

import java.io.ByteArrayInputStream;
import java.io.ObjectInputStream;

import ro.pub.cs.taxi.R;
import ro.pub.cs.taxi.server.passenger.TaxiRecommendation;
import ro.pub.cs.taxi.server.passenger.TaxiReview;
import android.app.Activity;
import android.content.Intent;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.os.Bundle;

public class TaxiDriverInfoActivity extends Activity implements View.OnClickListener {

	public static final int RESULT_CODE_SELECTED = 0;
	public static final int RESULT_CODE_UNSELECTED = 1;
	public static final int RESULT_CODE_CANCELED = 2;
	
	private Button select, cancel;
	private String mId;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_taxi_driver_info);
		Bundle extra = getIntent().getExtras();

		TaxiRecommendation rec = null;
		byte bytes[] = Base64.decode(extra.getString("str"), Base64.NO_WRAP);
		ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
		try {
			ObjectInputStream in = new ObjectInputStream(bis);
			rec = (TaxiRecommendation)in.readObject();
		}
		catch (Exception e) {
			e.printStackTrace();
		}

		mId = rec.getId();
		
		select = (Button)findViewById(R.id.select_taxi_driver_button);
		cancel = (Button)findViewById(R.id.cancel_taxi_driver_button);

		((TextView)findViewById(R.id.taxi_driver_text_id)).setText("Driver ID: " + mId);
		((TextView)findViewById(R.id.taxi_driver_text_name)).setText("Driver username: " + rec.getName());

	   	TextView textView = (TextView)findViewById(R.id.reviews);
	   	textView.setTextSize(20);
	   	StringBuffer buff = new StringBuffer();
		for (TaxiReview tr : rec.getReviews()) {
			buff.append(tr.getName());
			buff.append("\n");
			buff.append(tr.getText());
			buff.append("\n\n");
		}		
		textView.setText(buff.toString());
		
		select.setOnClickListener(this);
		cancel.setOnClickListener(this);
	}

	@Override
	public void onClick(View view) {
		if (select.equals(view)) {
			Intent i = new Intent();
			i.putExtra("id", mId);
			setResult(RESULT_CODE_SELECTED, i);
			finish();
		}
		else if (cancel.equals(view)) {
			Intent i = new Intent();
			i.putExtra("id", mId);
			setResult(RESULT_CODE_UNSELECTED, i);
			finish();
		}
	}
}
