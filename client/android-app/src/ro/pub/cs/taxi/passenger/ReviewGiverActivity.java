package ro.pub.cs.taxi.passenger;

import ro.pub.cs.taxi.R;
import ro.pub.cs.taxi.server.IPResolver;
import ro.pub.cs.taxi.server.async.Continuation;
import ro.pub.cs.taxi.server.passenger.TaxiCallerManagerImpl;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class ReviewGiverActivity extends Activity implements OnClickListener, Continuation<Boolean> {

	Button button;
	EditText text;
	TaxiCallerManagerImpl mTaxiCaller;
	private String reviewId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		reviewId = PassengerActivity.loadReviewID(this);
		if (reviewId == null) {
			finish();
			return;
		}

		setContentView(R.layout.activity_review_giver);

		button = (Button) findViewById(R.id.rev_add);
		text = (EditText) findViewById(R.id.rev_text);

		button.setOnClickListener(this);
		
		mTaxiCaller = new TaxiCallerManagerImpl(new IPResolver(), 8080);
	}

	@Override
	public void onClick(View view) {
		String content = text.getText().toString();
		mTaxiCaller.addReview(reviewId, content, this);
	}

	@Override
	public void actionFinished(boolean success, Boolean result) {
		if (success) {
			PassengerActivity.saveReviewID(this, null);
			finish();
		}
	}

}
