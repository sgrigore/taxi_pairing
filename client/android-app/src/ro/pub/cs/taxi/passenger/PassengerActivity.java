package ro.pub.cs.taxi.passenger;

import java.io.ByteArrayOutputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.Session.StatusCallback;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import ro.pub.cs.taxi.R;
import ro.pub.cs.taxi.driver.location.LocationResolver;
import ro.pub.cs.taxi.facebook.FacebookPictureMarker;
import ro.pub.cs.taxi.server.Coords;
import ro.pub.cs.taxi.server.IPResolver;
import ro.pub.cs.taxi.server.async.Continuation;
import ro.pub.cs.taxi.server.passenger.TaxiCallerManagerImpl;
import ro.pub.cs.taxi.server.passenger.TaxiRecommendation;
import ro.pub.cs.taxi.server.passenger.TaxiStatus;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class PassengerActivity extends FragmentActivity implements LocationListener,
								OnMarkerClickListener, OnInfoWindowClickListener, View.OnClickListener,
								OnMapLongClickListener, StatusCallback {
	
	private static final int TAXI_DRIVER_ACTIVITY_CODE = 0;
	private static String TAG = "PassengerActivity";
	
	private int mState;
	private static final int STATE_INITIAL = 0;
	private static final int STATE_LOCATION_SET = 1;
	private static final int STATE_TAXI_SELECTED = 2;
	private static final int STATE_REQUST_MADE = 3;
	private static final int STATE_WAIT_FOR_RESPONSE = 4;
	private static final int STATE_WAIT_FOR_TAXI = 5;
	private static final int STATE_TAXI_ARRIVED = 6;
	
	private LocationManager mLocationManager;
	private LocationResolver mLocationResolver;
	private String mLocationProvider;
	private GoogleMap mMap;
	private Location mMyLocation;
	private LatLng mDestination;
	private FacebookPictureMarker mMyLocationMarker;
	private Marker mDestinationMarker;
	private TaxiCallerManagerImpl mTaxiCaller;
	private PassengerActivity mActivity;

	private String mRequestKey;
	private String mRequestId;
	
	private HashMap<String, Marker> mTaxiMarkers;
	private HashMap<Marker,TaxiRecommendation> mMarkerMap;
	private HashSet<String> mSelectedTaxis;
	private TaxiStatus mCalledTaxi;
	private Marker mCalledTaxiMarker;
	private Marker mLastClickedMarker;
		
	private LoginButton facebookLogin;
	private UiLifecycleHelper uiHelper;
	private StatusCallback facebookListener;
	private String fuid;
	private String facebookToken;
	
	private Button reviewButton;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_passenger_main);
		mActivity = this;
		mState = STATE_INITIAL;
		
		reviewButton = (Button) findViewById(R.id.add_review_button);
		
		// facebook login button
		fuid = null;
		facebookToken = null;
		facebookListener = null;
		facebookLogin = (LoginButton) findViewById(R.id.facebook_login_button);
		uiHelper = new UiLifecycleHelper(this, this);
		uiHelper.onCreate(savedInstanceState);
		if (Session.getActiveSession() != null) {
			call(Session.getActiveSession(), null, null);
		}
		
		reviewButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(PassengerActivity.this, ReviewGiverActivity.class);
				startActivity(i);
			}
		});
		
		// get map object
		mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
	    mMap.setMyLocationEnabled(true);
	    mMyLocationMarker = null;
	    mMyLocation = null;
	    mDestination = null;
	    mDestinationMarker = null;
	    mCalledTaxi = null;
	    mCalledTaxiMarker = null;
	    mLastClickedMarker = null;
	    
	    mMap.setOnMapLongClickListener(this);
	    mMap.setOnMarkerClickListener(this);
		
		// Enable GPS
		mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) &&
				!mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
			Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
			startActivity(intent);
		}
		
	    Criteria criteria = new Criteria();
	    mLocationProvider = mLocationManager.getBestProvider(criteria, false);
	    Location location = mLocationManager.getLastKnownLocation(mLocationProvider);
	    // Initialize the location fields
	    if (location != null) {
	        CameraUpdate update = CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),
	                					location.getLongitude()));
	        mMap.moveCamera(update);
	        mMap.animateCamera(CameraUpdateFactory.zoomTo(14));
	    	onLocationChanged(location);
	    }
	    
	    mTaxiCaller = new TaxiCallerManagerImpl(new IPResolver(), 8080);
	    mTaxiMarkers = new HashMap<String, Marker>();
	    mMarkerMap = new HashMap<Marker, TaxiRecommendation>();
	    mSelectedTaxis = new HashSet<String>();
	    
        Button button = (Button) findViewById(R.id.call_taxi_button);
        button.setOnClickListener(this);
        mLocationResolver = new LocationResolver(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		uiHelper.onResume();
		mLocationManager.requestLocationUpdates(mLocationProvider, 400, 1, this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		uiHelper.onPause();
		mLocationManager.removeUpdates(this);
	}

	@Override
	public void onLocationChanged(Location location) {
		if (mState == STATE_INITIAL && mMyLocation == null && location != null) {
			mState = STATE_LOCATION_SET;
		}
		
		if (mMyLocation != null && location != null) {
			if (new Coords(location.getLatitude(), location.getLongitude(), location.getAltitude())
				.getDistance(mMyLocation.getLatitude(), mMyLocation.getLongitude(), mMyLocation.getAltitude()) < 30) {
				return;
			}
		}
		
		mMyLocation = location;
		Log.d(TAG, "location: " + location.getLatitude() + " " + location.getLongitude());
		
		if (mMyLocationMarker != null) {
			mMyLocationMarker.getMarker().remove();
		}
		
		BitmapDescriptor myPic = null;
		myPic = BitmapDescriptorFactory.fromResource(R.drawable.person);
	    Marker mk = mMap.addMarker(new MarkerOptions()
	        .position(new LatLng(location.getLatitude(), location.getLongitude()))
	        .title("You")
	        .snippet("You are here")
	        .icon(myPic));
	    mMyLocationMarker = new FacebookPictureMarker(this, mk, fuid);
	}
	
	@Override
	public void onMapLongClick(LatLng ll) {
		if (mState != STATE_LOCATION_SET && mState != STATE_TAXI_SELECTED)
			return;
		
		if (mDestinationMarker != null) {
			mDestinationMarker.remove();
		}
		mDestination = ll;
		mDestinationMarker = mMap.addMarker(new MarkerOptions()
		    	.position(ll)
		    	.title("Destination")
		    	.icon(BitmapDescriptorFactory.fromResource(R.drawable.treasure)));
		mLocationResolver.setMarkerAddress(ll.latitude, ll.longitude, mDestinationMarker);
	}

	@Override
	public void onClick(View v) {
		switch (mState) {
		case STATE_INITIAL:
			showToast("Need to determine own location first");
			return;
		case STATE_LOCATION_SET:
			mTaxiCaller.getNearbyTaxi(new Coords(mMyLocation.getLatitude(), mMyLocation.getLongitude(), mMyLocation.getAltitude()), facebookToken,
				new Continuation<List<TaxiRecommendation>>() {
					@Override
					public void actionFinished(boolean success, List<TaxiRecommendation> result) {
						if (!success) {
							mActivity.showToast("Failed to get nearby taxi list");
							return;
						}
						
						for (final TaxiRecommendation tr : result) {
							Log.d(TAG, "Taxi at:" + tr.getCoords().getLatitude() + " " + tr.getCoords().getLongitude());
							mActivity.runOnUiThread(new Runnable() {
								public void run() {
									Marker m = mTaxiMarkers.get(tr.getId());
									LatLng ll = new LatLng(tr.getCoords().getLatitude(), tr.getCoords().getLongitude());
									if (m != null) {
										m.setPosition(ll);
										return;
									}
									m = mMap.addMarker(new MarkerOptions()
	        				        	.position(ll)
	        				        	.title(tr.getName())
	        				        	.icon(BitmapDescriptorFactory.fromResource(R.drawable.taxi_unselected)));
	        						mTaxiMarkers.put(tr.getId(), m);
	        						mMarkerMap.put(m, tr);	
								}
							});
						}
					}
				}
			);
			mMap.setOnInfoWindowClickListener(this);
			break;
		case STATE_TAXI_SELECTED:
			mState = STATE_REQUST_MADE;
			mRequestKey = UUID.randomUUID().toString().replaceAll("-", "");
			Coords src = new Coords(mMyLocation.getLatitude(), mMyLocation.getLongitude(), mMyLocation.getAltitude());
			Coords dest = mDestination == null ? null : new Coords(mDestination.latitude, mDestination.longitude, 0);
			mTaxiCaller.makeRequest(src, dest, mSelectedTaxis, mRequestKey,
				new Continuation<String>() {
					@Override
					public void actionFinished(boolean success, String result) {
						if (success == false) {
							mActivity.showToast("Error requesting taxi!");
							return;
						}
						mRequestId = result;
						mState = STATE_WAIT_FOR_RESPONSE;
						mTaxiCaller.waitForReady(mRequestId, mRequestKey, fuid,
							new Continuation<TaxiStatus>() {
								@Override
								public void actionFinished(boolean success, TaxiStatus result) {
									if (success == false) {
										mActivity.showToast("Error waiting for req response!");
										return;
									}
										
									mState = STATE_WAIT_FOR_TAXI;
									mCalledTaxi = result;
									mCalledTaxiMarker = mTaxiMarkers.get(mCalledTaxi.getID());

									saveReviewID(PassengerActivity.this, mCalledTaxi.getReviewID());
									
									mActivity.runOnUiThread(new Runnable(){
										@Override
										public void run() {
											for (Marker m : mMarkerMap.keySet()) {
												if (m.equals(mCalledTaxiMarker))
													continue;
												m.remove();
											}		
										}
									});
									
									mMarkerMap.clear();
									mTaxiMarkers.clear();
									
									while (true) {
										try {
											final Coords c = mTaxiCaller.getTaxiCaller().query_location(mCalledTaxi);
											if (c == null) {
												return;
											}

											mActivity.runOnUiThread(new Runnable(){
												@Override
												public void run() {
													mCalledTaxiMarker.setPosition(new LatLng(c.getLatitude(), c.getLongitude()));		
												}
											});
											
											if (c.getDistance(mMyLocation.getLatitude(), mMyLocation.getLongitude(), 
													mMyLocation.getAltitude()) < 50) {
												mState = STATE_TAXI_ARRIVED;
												mActivity.showToast("Taxi " + mCalledTaxi.getRegNumber() + " has arrived");
												return;
											}
											
											Thread.sleep(TaxiCallerManagerImpl.QUERY_INTERVAL);
										}
										catch (Exception e) {
											mActivity.showToast("Error getting taxi location!");
											break;
										}
									}
								}
							}
						);
					}		
				}
			);
		}
   	}
	

	@Override
	public boolean onMarkerClick(Marker m) {
		if (m.equals(mDestinationMarker) || m.equals(mMyLocationMarker.getMarker()))
			return false;
		if (mLastClickedMarker == null || mLastClickedMarker.equals(m) == false) {
			mLastClickedMarker = m;
			return false;
		}
		
		String id = mMarkerMap.get(m).getId();
		int resultCode = mSelectedTaxis.contains(id) ? 
					TaxiDriverInfoActivity.RESULT_CODE_UNSELECTED :
					TaxiDriverInfoActivity.RESULT_CODE_SELECTED;
		
		doSelectTaxiMarker(id, m, resultCode);
		
		return false;
	}

	@Override
	public void onInfoWindowClick (Marker m) {
		if (m.equals(mMyLocationMarker) || m.equals(mDestinationMarker)) {
			return;
		}

		if (mState != STATE_LOCATION_SET && mState != STATE_TAXI_SELECTED) {
			return;
		}

		String str = null;
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try {
			out = new ObjectOutputStream(bos);
			out.writeObject(mMarkerMap.get(m));
			str = Base64.encodeToString(bos.toByteArray(), Base64.NO_WRAP);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		Intent intent = new Intent(this, TaxiDriverInfoActivity.class);
		intent.putExtra("str", str);
		startActivityForResult(intent, TAXI_DRIVER_ACTIVITY_CODE);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == TAXI_DRIVER_ACTIVITY_CODE) {
			if (data == null)
				return;
			String id = data.getExtras().getString("id");
			Marker m = mTaxiMarkers.get(id);
			doSelectTaxiMarker(id, m, resultCode);
		}
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}
	
	private void doSelectTaxiMarker(String id, Marker m, int resultCode) {
		if (resultCode == TaxiDriverInfoActivity.RESULT_CODE_SELECTED) {
			m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.taxi_selected));
			mSelectedTaxis.add(id);
		}
		else if (resultCode == TaxiDriverInfoActivity.RESULT_CODE_UNSELECTED) {
			m.setIcon(BitmapDescriptorFactory.fromResource(R.drawable.taxi_unselected));
			mSelectedTaxis.remove(id);
		}

		if (mState == STATE_LOCATION_SET && mSelectedTaxis.size() > 0) {
			mState = STATE_TAXI_SELECTED;
	        Button button = (Button) findViewById(R.id.call_taxi_button);
	        button.setText(R.string.call_taxi);
		}

		if (mState == STATE_TAXI_SELECTED && mSelectedTaxis.size() == 0) {
			mState = STATE_LOCATION_SET;
	        Button button = (Button) findViewById(R.id.call_taxi_button);
	        button.setText(R.string.get_nearby_taxi);
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}
	
	@Override
	public void onProviderDisabled(String provider) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extra) {
	}
	
	public void showToast(final String msg) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
	   			Toast.makeText(mActivity, msg, Toast.LENGTH_SHORT).show();
			}
		});
	}

	public void setFacebookListener(Session.StatusCallback listener) {
		facebookListener = listener;
	}
	
	@Override
	public void call(Session session, SessionState state, Exception exception) {
		if (facebookListener != null)
		{
			facebookListener.call(session, state, exception);
		}
		showFacebookButton(session == null || !session.isOpened());
		updateFacebookUid();
		if (session != null && session.isOpened()) {
			facebookToken = session.getAccessToken();
		}
		else {
			facebookToken = null;
		}
	}
	
	private void showFacebookButton(final boolean val) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				facebookLogin.setVisibility(val ? View.VISIBLE : View.GONE);
				facebookLogin.requestLayout();
				facebookLogin.invalidate();
				reviewButton.setVisibility(!val ? View.VISIBLE : View.GONE);
				reviewButton.requestLayout();
				reviewButton.invalidate();
			}
		});
	}
	
	private void updateFacebookUid() {
		final Session session = Session.getActiveSession();
		if (session == null || !session.isOpened()) {
			fuid = null;
			return;
		}
		if (fuid != null) {
			return;
		}
		Request request = Request.newMeRequest(session, new Request.GraphUserCallback() {
			@Override
			public void onCompleted(GraphUser user, Response response) {
				if (session == Session.getActiveSession()) {
					if (user != null) {
						fuid = user.getId();
						mMyLocationMarker.setFuid(fuid);
					}
				}
			}
		});
		Request.executeBatchAsync(request);
	}

	private static final String PREFS_NAME = "TaxiReviewStore";
	static void saveReviewID(Context context, String id) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		SharedPreferences.Editor editor = settings.edit();
		editor.putString("reviewID", id);
		editor.commit();
	}
	static String loadReviewID(Context context) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		return settings.getString("reviewID", null);
	}
}